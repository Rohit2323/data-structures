#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

// Program to create Binary Search Tree from Preorder given tranversal

struct Node
{
	struct Node *lchild;
	int data;
	struct Node *rchild;
}*root = NULL;

/*********************** Stack *****************************/
struct Stack
{
	int size;
	int top;
	struct Node **S;
};

void StackCreate(struct Stack *st, int size)
{
	st->size = size;
	st->top = -1;
	st->S = (struct Node **)malloc(st->size * sizeof(struct Node *));
}

void push(struct Stack *st, struct Node *x)
{
	if (st->top == st->size - 1) {
		printf("Stack Overflow\n");
	} else {
		st->top++;
		st->S[st->top] = x;
	}
}

struct Node * pop(struct Stack *st)
{
	struct Node *x = NULL;

	if (st->top == -1) {
		printf("Stack Underflow\n");
	} else {
		x = st->S[st->top--];
	}
	return x;
}

int isEmptyStack(struct Stack st)
{
	if (st.top == -1) {
		return 1;
	}
	return 0;
}

int isFullStack(struct Stack st)
{
	return st.top == st.size - 1;
}

struct Node * StackTop(struct Stack st)
{
	if (!isEmptyStack(st)) {
		return st.S[st.top];
	}
	return NULL;
}

/*********************** Tree *****************************/
void createPre(int pre[], int n)
{
	struct Stack stk;
	struct Node *t, *p;
	int i =0;
	
	StackCreate(&stk, 100);

	root = (struct Node *)malloc(sizeof(struct Node));
	root->data = pre[i++];
	root->lchild = root->rchild = NULL;
	p = root;

	while (i < n) {
		if (pre[i] < p->data) {
			t = (struct Node *)malloc(sizeof(struct Node));
			t->data = pre[i++];
			t->lchild = t->rchild = NULL;
			p->lchild = t;
			push(&stk, p);
			p = t;
		} else {
			// (StackTop(stk) > (struct Node *)NULL ? StackTop(stk)->data : INT_MAX) checked if stacktop returns NULL then value is infinity
			if (pre[i] > p->data && pre[i] < (StackTop(stk) > (struct Node *)NULL ? StackTop(stk)->data : INT_MAX)) {
				t = (struct Node *)malloc(sizeof(struct Node));
				t->data = pre[i++];
				t->lchild = t->rchild = NULL;
				p->rchild = t;
				p = t;
			} else {
				p = pop(&stk);
			}
		}
	}
}

void Inorder(struct Node *p)
{
	if (p) {
		Inorder(p->lchild);
		printf("%d ", p->data);
		Inorder(p->rchild);
	}
}

int main()
{
	int pre[] = {30, 20, 10, 25, 40, 50, 45};

	createPre(pre, 7);

	Inorder(root);
	printf("\n");

	return 0;
}
