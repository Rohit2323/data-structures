#include <stdio.h>
#include <stdlib.h>

// Program to create Binary Search Tree

/* BST - Binary tree in which for every node, all the elements from left subtree are smaller than that node from
	and all the elements from right subtree are greater than that node.
	BST Time complexity
	a. Searching: log n < h < n
	min = log n
	max = n

	b. Creating BST:
	n log n

	Property:
	a. No duplicates
	b. Inorder gives sorted order
	n. No of BST for n nodes
	T(n) = 2nCn/n+1
	Example: n = 3 5 BST possible
*/

struct Node
{
	struct Node *lchild;
	int data;
	struct Node *rchild;
}*root = NULL, *Rroot = NULL;

/* Iterative Insert() */
void Insert(int key)
{
	struct Node *t = root;
	struct Node *r, *p;

	if (root == NULL) {
		p = (struct Node *)malloc(sizeof(struct Node));
		p->data = key;
		p->lchild = p->rchild = NULL;
		root = p;
		return;
	}

	while (t != NULL) {
		r = t;

		if (key < t->data) {
			t = t->lchild;
		} else if (key > t->data) {
			t = t->rchild;
		} else {
			return; // Key is already present in BST
		}
	}
	p = (struct Node *)malloc(sizeof(struct Node));
	p->data = key;
	p->lchild = p->rchild = NULL;

	if (key < r->data) {
		r->lchild = p; // r is tailing pointer
	} else {
		r->rchild = p;
	}
}

/* Iterative Search() */
struct Node * Search(int key)
{
	struct Node *t = root;

	while (t != NULL) {
		if (key == t->data) {
			return t;
		} else if (key < t->data) {
			t = t->lchild;
		} else {
			t = t->rchild;
		}
	}
	return NULL;
}

/* Iterative Insert() */
struct Node *RInsert(struct Node *p, int key)
{
	struct Node *t;

	if (p == NULL) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = key;
		t->lchild = t->rchild = NULL;
		return t;
	}

	if (key == p->data) {
		return NULL;
	} else if (key < p->data) {
		// Assignment to pointer is important
		p->lchild = RInsert(p->lchild, key);
	} else if (key > p->data) {
		p->rchild = RInsert(p->rchild, key);
	}

	return p;
}

/* Recursive Search() */
struct Node * RSearch(struct Node *p, int key)
{
	struct Node *t;

	if (p == NULL) {
		return NULL;
	}

	if (key == p->data) {
		return p;
	} else if (key < p->data) {
		t = RSearch(p->lchild, key);
	} else if (key > p->data) {
		t = RSearch(p->rchild, key);
	}
}

void Inorder(struct Node *p)
{
	if (p) {
		Inorder(p->lchild);
		printf("%d ", p->data);
		Inorder(p->rchild);
	}
}

int Height(struct Node *p)
{
	int x = 0, y = 0;

	if (p == NULL) {
		return 0;
	}
	x = Height(p->lchild);
	y = Height(p->rchild);
	return x > y ? x + 1 : y + 1;
}

/* Inoder Predecer is right most element of left sub-tree */
struct Node *InPre(struct Node *p)
{
	while (p && p->rchild != NULL) {
		p = p->rchild;
	}
	return p;
}

/* Inoder successer is left most element of right sub-tree */
struct Node *InSucc(struct Node *p)
{
	while (p && p->lchild != NULL) {
		p = p->lchild;
	}
	return p;
}

struct Node * Delete(struct Node *p, int key)
{
	struct Node *q;

	if (p == NULL) {
		return NULL;
	}

	if (p->lchild == NULL && p->rchild == NULL) {
		if (p == root)
			root = NULL;

			free(p);
			return NULL;
	}

	if (key < p->data) {
		p->lchild = Delete(p->lchild, key);
	} else if (key > p->data) {
		p->rchild = Delete(p->rchild, key);
	} else {
		if (Height(p->lchild) > Height(p->rchild)) {
			q = InPre(p->lchild);
			p->data = q->data;
			p->lchild = Delete(p->lchild, q->data);
		} else {
			q = InSucc(p->rchild);
			p->data = q->data;
			p->rchild = Delete(p->rchild, q->data);
		}
	}
	return p;
}

int main()
{
	struct Node *temp;

	printf("Iterative Insert()\n");
	/*
	Insert(10);
	Insert(5);
	Insert(20);
	Insert(8);
	Insert(30);
	*/
	
	Insert(50);
	Insert(10);
	Insert(40);
	Insert(20);
	Insert(30);

	printf("Inorder : ");
	Inorder(root);
	printf("\n");

	temp = Search(20);
	if (temp) {
		printf("Element %d is found\n", temp->data);
	} else {
		printf("Element is not found\n");
	}
	
	temp = Search(25);
	if (temp) {
		printf("Element %d is found\n", temp->data);
	} else {
		printf("Element 25 is not found\n");
	}

	printf("Delete 50\n");
	Delete(root, 50);
	printf("Inorder : ");
	Inorder(root);
	printf("\n");
	
	printf("Recursive Insert()\n");
	Rroot = RInsert(Rroot, 10);
	RInsert(Rroot, 5);
	RInsert(Rroot, 20);
	RInsert(Rroot, 8);
	RInsert(Rroot, 30);
	
	printf("Inorder : ");
	Inorder(root);
	printf("\n");
	
	temp = RSearch(Rroot, 20);
	if (temp) {
		printf("Element %d is found\n", temp->data);
	} else {
		printf("Element is not found\n");
	}
	
	temp = RSearch(Rroot, 25);
	if (temp) {
		printf("Element %d is found\n", temp->data);
	} else {
		printf("Element 25 is not found\n");
	}

	return 0;
}
