#include <stdio.h>

// Program to change upper case to lower case and vise versa

// ASCII Values
// A = 65, Z = 90
// a = 97, z = 122

void upper_lower_case(char *s)
{
	int i;

	for (i = 0; s[i] != '\0'; i++) {
		if (s[i] >= 'A' && s[i] <= 'Z') {
			s[i] += 32;
		} else if (s[i] >= 'a' && s[i] <= 'z') {
			s[i] -= 32;
		}
	}
	printf("O/P string : %s\n", s);
}

int main()
{
	char A[] = "wElCome";

	upper_lower_case(A);

	return 0;
}
