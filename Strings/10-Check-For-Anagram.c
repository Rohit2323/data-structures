#include <stdio.h>

// Program to check whether given 2 strings are Anagram or not
// Anagram means to strings having same set of alphabates
// Example: decimal:medical, verbose:observe
// Time complexity: n

void check_anagram(char *A, char *B)
{
	int i, H[26] = { 0 };

	for (i = 0; A[i] != '\0'; i++) {
		H[A[i] - 97] += 1;
	}
	
	for (i = 0; B[i] != '\0'; i++) {
		H[B[i] - 97] -= 1;
		if (H[B[i] - 97] < 0) {
			printf("%s and %s are not anagram strings\n", A, B);
			break;
		}
	}

	if (B[i] == '\0') {
		printf("%s and %s are anagram strings\n", A, B);
	}
}

int main()
{
	char A[] = "decimal";
	char B[] = "medical";
	char C[] = "verbose";
	char D[] = "observe";

	check_anagram(A, B);
	check_anagram(C, D);
	check_anagram(A, C);

	return 0;
}
