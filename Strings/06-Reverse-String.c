#include <stdio.h>

// Program to reverse the string

void rev_method1(char *s)
{
	int i, j;

	for (i = 0; s[i] != '\0'; i++) {
	}
	i = i - 1;
	char B[i];

	for (j = 0; i >= 0; i--, j++) {
		B[j] = s[i];
	}
	B[j] = '\0';
	printf("Reverse String: %s\n", B);
}

void rev_method2(char *s)
{
	int i, j;
	char t;

	for (j = 0; s[j] != '\0'; j++) {
	}
	j = j - 1;

	for (i = 0; i < j; i++, j--) {
		t = s[i];
		s[i] = s[j];
		s[j] = t;
	}
	printf("Reverse String: %s\n", s);
}

int main()
{
	char A[] = "Python";
	char B[] = "Python";

	rev_method1(A);
	rev_method2(B);
	return 0;
}
