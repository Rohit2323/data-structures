#include <stdio.h>

// Program to compare two strings whether the are equal or not

void str_cmp(char *A, char *B)
{
	int i, j;

	for (i = 0, j = 0; A[i] != '\0' && B[j] != '\0'; i++, j++) {
		if (A[i] != B[j]) {
			break;
		}
	}

	if (A[i] == B[j]) {
		printf("Equal: A: %s B: %s\n", A, B);
	} else if (A[i] < B[j]) {
		printf("Smaller: A: %s B: %s\n", A, B);
	} else {
		printf("Greater: A: %s B: %s\n", A, B);
	}
}

int main()
{
	char A[] = "Painter";
	char B[] = "Painter";
	//char B[] = "Painting";

	str_cmp(A, B);

	return 0;
}
