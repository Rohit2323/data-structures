#include <stdio.h>
#include <string.h>

// Program to find duplicates in given string
// Assumption given string is in lower case
// Time complexity : n

int method1_hashtable(char *A)
{
	int H[26], i;

	printf("Finding duplicates using HashTable\n");

	memset(H, 0, sizeof(H));

	for (int i = 0; A[i] != '\0'; i++) {
		H[A[i] - 97] += 1;
	}

	for (i = 0; i < 26; i++) {
		if (H[i] > 1) {
			printf("%c: ", i + 97);
			printf("%d\n", H[i]);
		}
	}
}

int method2_bitwise_operation(char *A)
{
	int H = 0, x = 0, i;
	
	printf("Finding duplicates using Bitwise Operation\n");

	for (i = 0; A[i] != '\0'; i++) {
		x = 1;
		x = x << A[i] - 97; // number of bit left shift
		if ((x & H) > 0) { // masking
			printf("%c is duplicate\n", A[i]);
		} else {
			H = x | H; // bit merging
		}
	}
}

int main()
{
	char A[] = "finding";

	method1_hashtable(A);
	method2_bitwise_operation(A);

	return 0;
}
