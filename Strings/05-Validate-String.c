#include <stdio.h>

// Program to validate string, only Alphanumeric characters are allowed

int valid_string(char *s)
{
	int i;

	for (i = 0; s[i] != '\0'; i++) {
		if (!(s[i] >= 'A' && s[i] <= 'Z') && !(s[i] >= 'a' && s[i] <= 'z') && !(s[i] >= 48 && s[i] <= 57)) {
			return 0;
		}
	}
	return 1;
}

int main()
{
	//char *name = "Ani?321";
	char *name = "Anil321";

	if (valid_string(name)) {
		printf("Valid I/P string : %s\n", name);
	} else {
		printf("Invalid I/P string : %s\n", name);
	}
	return 0;
}
