#include <stdio.h>

// Program to count vowel and consonents in given string

int main()
{
	char A[] = "How are you";
	int i, v_count = 0, c_count = 0;

	for (i = 0; A[i] != '\0'; i++) {
		if (A[i] == 'a' || A[i] == 'e' || A[i] == 'i' || A[i] == 'o' || A[i] == 'u'
			|| A[i] == 'A' || A[i] == 'E' || A[i] == 'I' || A[i] == 'O' || A[i] == 'U') {
			v_count++;
		} else if ((A[i] >= 'A' && A[i] <= 'Z') || (A[i] >= 'a' && A[i] <= 'z')) {
			c_count++;
		}
	}

	printf("I/P string : %s\n", A);
	printf("Vowel count : %d Consonent count : %d\n", v_count, c_count);

	return 0;
}
