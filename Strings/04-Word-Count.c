#include <stdio.h>

// Program to count words in given string, consider white spaces as well

void word_count(char *A) {
	int i, word = 1;

	for (i = 0; A[i] != '\0'; i++) {
		if (A[i] == ' ' && A[i - 1] != ' ') {
			word++;
		}
	}
	printf("I/P string : %s\n", A);
	printf("Total words : %d\n", word);
}

int main()
{
	char A[] = "How are you";
	char B[] = "How  are you";

	word_count(A);
	word_count(B);
	return 0;
}
