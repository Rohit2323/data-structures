#include <iostream>
using namespace std;

// Program to add 2 sparse matrix
// Sparse matrix means having too few no-zero elements in it.
// 3-column representation
// Size of matrix is m * n and total non-zero elements is = num

/* Example  m = 4 n = 5 num = 5 non-zero elements
 * i 4  1 2 3 4 5 
 * j 5  1 2 0 3 4
 * x 5  1 3 4 1 2
 * sum->m = s1->m
 * sum->n = s1->n
 * sum->ele = s1->num+s2->num max
 */

/*
First Matrix
1 0 0 0 0 
0 1 0 0 0 
0 0 1 0 0 
0 0 0 1 0 
0 0 0 0 1 
Second Matrix
2 0 0 0 0 
2 0 0 0 0 
2 0 0 0 0 
2 0 0 0 0 
2 0 0 0 0 
Sum Matrix
3 0 0 0 0 
2 1 0 0 0 
2 0 1 0 0 
2 0 0 1 0 
2 0 0 0 1
*/

/* Operator overloading
 * 1. As Friend Function
 * <<, >>
 * 2. As a member function
 * +
 */

class Element
{
	public:
		int i;
		int j;
		int x;
};

class Sparse
{
	private:
		int m;
		int n;
		int num;
		Element *ele;

	public:
		Sparse(int m, int n, int num)
		{
			this->m = m;
			this->n = n;
			this->num = num;
			ele = new Element[this->num];
		}

		~Sparse()
		{
			delete [] ele;
		}

		Sparse operator+(Sparse &s);

		friend istream & operator>>(istream &is, Sparse &s);
		friend ostream & operator<<(ostream &os, Sparse &s);
};
		
Sparse Sparse::operator+(Sparse &s)
{
	int i, j, k;

	Sparse *sum = new Sparse(m, n, num + s.num);
	i = j = k = 0;
	while (i < num && j < s.num) {
		if (ele[i].i < s.ele[j].i) {
			sum->ele[k++] = ele[i++];
		} else if (ele[i].i > s.ele[j].i) {
			sum->ele[k++] = s.ele[j++];
		} else {
			if (ele[i].j < s.ele[j].j) {
				sum->ele[k++] = s.ele[i++];
			} else if (ele[i].j > s.ele[j].j) {
				sum->ele[k++] = s.ele[j++];
			} else {
				sum->ele[k] = ele[i];
				sum->ele[k++].x = ele[i++].x + s.ele[j++].x;
			}
		}
	}
	for (; i < num; i++) sum->ele[k++] = ele[i];
	for (; j < s.num; j++) sum->ele[k++] = s.ele[j];

	sum->m = m;
	sum->n = n;
	sum->num = k;

	return *sum;
}

istream & operator>>(istream &is, Sparse &s)
{
	cout << "Enter non-zero elements" << endl;
	for (int i = 0; i < s.num; i++) {
		cin >> s.ele[i].i >> s.ele[i].j >> s.ele[i].x;
	}
}

ostream & operator<<(ostream &os, Sparse &s)
{
	int i ,j, k = 0;

	for (i = 0; i < s.m; i++) {
		for (j = 0; j < s.n; j++) {
			if (i == s.ele[k].i && j == s.ele[k].j) {
				cout << s.ele[k++].x << " ";
			} else {
				cout << "0 ";
			}
		}
		cout << endl;
	}
}

int main()
{
	Sparse s1(5, 5, 5);
	Sparse s2(5, 5, 5);

	cin >> s1;
	cin >> s2;

	Sparse sum = s1 + s2;

	cout << "First Matrix" << endl << s1;
	cout << "Second Matrix" << endl << s2;
	cout << "Sum Matrix" << endl << sum;

	return 0;
}
