#include <iostream>
using namespace std;

// Program to create sparse matrix
// Sparse matrix means having too few no-zero elements in it.
// 3-column representation
// Size of matrix is m * n and total non-zero elements is = num

/* Example  m = 4 n = 5 num = 5 non-zero elements
 * i 4  1 2 3 4 5 
 * j 5  1 2 0 3 4
 * x 5  1 3 4 1 2
 */

class Element
{
	public:
		int i;
		int j;
		int x;
};

class Sparse
{
	private:
		int m;
		int n;
		int num;
		Element *ele;

	public:
		Sparse(int m, int n, int num)
		{
			this->m = m;
			this->n = n;
			this->num = num;
			ele = new Element[this->num];
		}

		~Sparse()
		{
			delete [] ele;
		}

		void read()
		{
			cout << "Enter non-zero elements" << endl;
			for (int i = 0; i < num; i++) {
				cin >> ele[i].i >> ele[i].j >> ele[i].x;
			}
		}
		
		void display()
		{
			int i ,j, k = 0;

			for (i = 0; i < m; i++) {
				for (j = 0; j < n; j++) {
					if (i == ele[k].i && j == ele[k].j) {
						cout << ele[k++].x << " ";
					} else {
						cout << "0 ";
					}
				}
				cout << endl;
			}
		}
};


int main()
{
	Sparse s(5, 5, 5);

	s.read();
	s.display();

	return 0;
}
