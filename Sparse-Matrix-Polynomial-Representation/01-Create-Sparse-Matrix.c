#include <stdio.h>
#include <stdlib.h>

// Program to create sparse matrix
// Sparse matrix means having too few no-zero elements in it.
// 3-column representation
// Size of matrix is m * n and total non-zero elements is = num

/* Example  m = 4 n = 5 num = 5 non-zero elements
 * i 4  1 2 3 4 5 
 * j 5  1 2 0 3 4
 * x 5  1 3 4 1 2
 */

struct Element
{
	int i;
	int j;
	int x;
};

struct Sparse
{
	int m;
	int n;
	int num;
	struct Element *ele;
};

void create(struct Sparse *s)
{
	int i;

	printf("Enter Dimensions m & n: ");
	scanf("%d%d", &s->m, &s->n);
	printf("Number of non-zero : ");
	scanf("%d", &s->num);

	s->ele = (struct Element *)malloc(s->num * sizeof(struct Element));
	printf("Enter non-zero Elements\n");
	for (i = 0; i < s->num; i++) {
		scanf("%d%d%d", &s->ele[i].i, &s->ele[i].j, &s->ele[i].x);
	}
}

void display(struct Sparse s)
{
	int i, j, k = 0;

	for (i = 0; i < s.m; i++) {
		for (j = 0; j < s.n; j++) {
			if (i == s.ele[k].i && j == s.ele[k].j) {
				printf("%d ", s.ele[k++].x);
			} else {
				printf("0 ");
			}
		}
		printf("\n");
	}
}

int main()
{
	struct Sparse s;

	create(&s);
	display(s);

	free(s.ele);

	return 0;
}
