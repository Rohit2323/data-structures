#include <stdio.h>
#include <stdlib.h>

// Program to add 2 sparse matrix
// Sparse matrix means having too few no-zero elements in it.
// 3-column representation
// Size of matrix is m * n and total non-zero elements is = num

/* Example  m = 4 n = 5 num = 5 non-zero elements
 * i 4  1 2 3 4 5 
 * j 5  1 2 0 3 4
 * x 5  1 3 4 1 2
 * sum->m = s1->m
 * sum->n = s1->n
 * sum->ele = s1->num+s2->num max
 */

/*
First Matrix
1 0 0 0 0 
0 1 0 0 0 
0 0 1 0 0 
0 0 0 1 0 
0 0 0 0 1 
Second Matrix
2 0 0 0 0 
2 0 0 0 0 
2 0 0 0 0 
2 0 0 0 0 
2 0 0 0 0 
Sum Matrix
3 0 0 0 0 
2 1 0 0 0 
2 0 1 0 0 
2 0 0 1 0 
2 0 0 0 1
*/

struct Element
{
	int i;
	int j;
	int x;
};

struct Sparse
{
	int m;
	int n;
	int num;
	struct Element *ele;
};

void create(struct Sparse *s)
{
	int i;

	printf("Enter Dimensions m & n: ");
	scanf("%d%d", &s->m, &s->n);
	printf("Number of non-zero : ");
	scanf("%d", &s->num);

	s->ele = (struct Element *)malloc(s->num * sizeof(struct Element));
	printf("Enter non-zero Elements\n");
	for (i = 0; i < s->num; i++) {
		scanf("%d%d%d", &s->ele[i].i, &s->ele[i].j, &s->ele[i].x);
	}
}

void display(struct Sparse s)
{
	int i, j, k = 0;

	for (i = 0; i < s.m; i++) {
		for (j = 0; j < s.n; j++) {
			if (i == s.ele[k].i && j == s.ele[k].j) {
				printf("%d ", s.ele[k++].x);
			} else {
				printf("0 ");
			}
		}
		printf("\n");
	}
}

struct Sparse * add(struct Sparse *s1 ,struct Sparse *s2)
{
	struct Sparse *sum;
	int i, j, k;
	i = j = k = 0;

	sum = (struct Sparse *)malloc(sizeof(struct Sparse));
	sum->ele = (struct Element *)malloc((s1->num +s2->num) * sizeof(struct Element));

	while (i < s1->num && j < s2->num) {
		if (s1->ele[i].i < s2->ele[j].i) {
			sum->ele[k++] = s1->ele[i++];
		} else if (s1->ele[i].i > s2->ele[j].i) {
			sum->ele[k++] = s2->ele[j++];
		} else {
			if (s1->ele[i].j < s2->ele[j].j) {
				sum->ele[k++] = s1->ele[i++];
			} else if (s1->ele[i].j > s2->ele[j].j) {
				sum->ele[k++] = s2->ele[j++];
			} else {
				sum->ele[k] = s1->ele[i];
				sum->ele[k++].x = s1->ele[i++].x + s2->ele[j++].x;
			}
		}
	}
	for (; i < s1->num; i++) sum->ele[k++] = s1->ele[i];
	for (; j < s2->num; j++) sum->ele[k++] = s2->ele[j];

	sum->m = s1->m;
	sum->n = s1->n;
	sum->num = k;

	return sum;
}

int main()
{
	struct Sparse s1, s2, *s3;

	create(&s1);
	create(&s2);

	s3 = add(&s1, &s2);

	printf("First Matrix\n");
	display(s1);
	printf("Second Matrix\n");
	display(s2);
	printf("Sum Matrix\n");
	display(*s3);

	free(s1.ele);
	free(s2.ele);
	free(s3->ele);
	free(s3);

	return 0;
}
