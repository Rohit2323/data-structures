#include <stdio.h>
#include <stdlib.h>

// Program to create doubly circular linked list

struct Node
{
	struct Node *prev;
	int data;
	struct Node *next;
}*Head = NULL;

void create(int A[], int n)
{
	int i;
	struct Node *t, *last;

	Head = (struct Node *)malloc(sizeof(struct Node));
	Head->data = A[0];
	Head->prev = Head->next = Head;
	last = Head;
	printf("H:%u L:%u H->P:%u L->N:%u\n\n", Head, last, Head->prev, last->next);
	printf("Before for loop\n");

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = last->next;
		t->prev = last;
		last->next = t;
		last = t;
		Head->prev = t;
		printf("H:%u L:%u H->P:%u L->N:%u\n\n", Head, last, Head->prev, last->next);
	}
	printf("After for loop\n");
	printf("H:%u L:%u H->P:%u L->N:%u\n\n", Head, last, Head->prev, last->next);
}

void Display(struct Node *p)
{
	do {
		printf("%d ", p->data);
		p = p->next;
	} while (p != Head);
	printf("\n\n");
}

int Length(struct Node *p)
{
	int l = 0;

	do {
		l++;
		p = p->next;
	} while (p != Head);

	return l;
}

void Insert(struct Node *p, int index, int x)
{
	int i;
	struct Node *t;

	if (index < 0 || index > Length(p)) {
		return;
	}

	if (index == 0) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = x;
		t->next = Head;
		t->prev = Head->prev;
		Head->prev = t;
		t->prev->next = t;
		Head = t;
	} else {
		for (i = 0; i < index - 1; i++) {
			p = p->next;
		}
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = x;
		t->prev = p;
		t->next = p->next;
		p->next->prev = t;
		p->next = t;
	}
}

int Delete(struct Node *p, int index)
{
	int x = -1, i;
	
	if (index < 0 || index > Length(p)) {
		return -1;
	}

	if (index == 1) {
		Head = Head->next;
		if (Head) {
			Head->prev = p->prev;
			Head->prev->next = Head;
		}
		x = p->data;
		free(p);
	} else {
		for (i = 0; i < index - 1; i++) {
			p = p->next;
		}

		p->prev->next = p->next;
		p->next->prev = p->prev;
		x = p->data;
		free(p);
	}

	return x;
}

int main()
{
	int A[] = {10, 20, 30, 40, 50};

	create(A, 5);

	printf("Length is : %d\n", Length(Head));

	Display(Head);

	printf("Insert 25 at 2\n");
	Insert(Head, 2, 25);
	Display(Head);
	
	printf("Insert 35 at 6\n");
	Insert(Head, 6, 35);
	Display(Head);
	
	printf("Insert 45 at 0\n");
	Insert(Head, 0, 45);
	Display(Head);

	printf("Deleted element : %d\n", Delete(Head, 1));
	Display(Head);
	
	printf("Deleted element : %d\n", Delete(Head, 7));
	Display(Head);
	
	printf("Deleted element : %d\n", Delete(Head, 3));
	Display(Head);

	return 0;
}
