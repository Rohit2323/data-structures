#include <stdio.h>
#include <stdlib.h>

// Program to create doubly linked list

struct Node
{
	struct Node *prev;
	int data;
	struct Node *next;
}*first = NULL;

void create(int A[], int n)
{
	int i;
	struct Node *t, *last;

	first = (struct Node *)malloc(sizeof(struct Node));
	first->data = A[0];
	first->prev = first->next = NULL;
	last = first;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = last->next;
		t->prev = last;
		last->next = t;
		last = t;
	}
}

void Display(struct Node *p)
{
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n\n");
}

int Length(struct Node *p)
{
	int l = 0;

	while (p != NULL) {
		l++;
		p = p->next;
	}

	return l;
}

void Insert(struct Node *p, int index, int x)
{
	int i;
	struct Node *t;

	if (index < 0 || index > Length(p)) {
		return;
	}

	if (index == 0) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = x;
		t->prev = NULL;
		t->next = first;
		first->prev = t;
		first = t;
	} else {
		for (i = 0; i < index - 1; i++) {
			p = p->next;
		}
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = x;
		t->prev = p;
		t->next = p->next;
		if (p->next) {
			p->next->prev = t;
		}
		p->next = t;
	}
}

int Delete(struct Node *p, int index)
{
	int x = -1, i;
	
	if (index < 0 || index > Length(p)) {
		return -1;
	}

	if (index == 1) {
		first = first->next;
		if (first) {
			first->prev = NULL;
		}
		x = p->data;
		free(p);
	} else {
		for (i = 0; i < index - 1; i++) {
			p = p->next;
		}

		p->prev->next = p->next;
		if (p->next) {
			p->next->prev = p->prev;
		}
		x = p->data;
		free(p);
	}

	return x;
}

void Reverse(struct Node *p)
{
	struct Node *temp;

	while (p != NULL) {
		temp = p->next;
		p->next = p->prev;
		p->prev = temp;
		p = p->prev;
		if (p != NULL && p->next == NULL) {
			first = p;
		}
	}
}

int main()
{
	int A[] = {10, 20, 30, 40, 50};

	create(A, 5);

	printf("Length is : %d\n", Length(first));

	Display(first);

	printf("Insert 25 at 2\n");
	Insert(first, 2, 25);
	Display(first);
	
	printf("Insert 35 at 6\n");
	Insert(first, 6, 35);
	Display(first);
	
	printf("Insert 45 at 0\n");
	Insert(first, 0, 45);
	Display(first);

	printf("Deleted element : %d\n", Delete(first, 1));
	Display(first);
	
	printf("Deleted element : %d\n", Delete(first, 7));
	Display(first);
	
	printf("Deleted element : %d\n", Delete(first, 3));
	Display(first);

	printf("Reverse List\n");
	Reverse(first);
	Display(first);

	return 0;
}
