#include <stdio.h>
#include <stdlib.h>

// Program to check linked is having loop or linear

// Self referencial structure
struct Node
{
	int data;
	struct Node *next;
}*first = NULL, *second = NULL;

void create(int A[], int n)
{
	int i;
	struct Node *t, *last;

	first = (struct Node *)malloc(sizeof(struct Node));
	first->data = A[0];
	first->next = NULL;
	last = first;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = NULL;
		last->next = t;
		last = t;
	}
}

void create2(int A[], int n)
{
	int i;
	struct Node *t, *last;

	second = (struct Node *)malloc(sizeof(struct Node));
	second->data = A[0];
	second->next = NULL;
	last = second;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = NULL;
		last->next = t;
		last = t;
	}
}

void display(struct Node *p)
{
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

int isLoop(struct Node *f)
{
	struct Node *p, *q;
	p = q = f;

	do {
		p = p->next;
		q = q->next;
		q = q ? q->next : q;
	} while (p && q && p != q);

	if (p == q) {
		return 1;
	} else {
		return 0;
	}
}


int main()
{
	struct Node *t1, *t2;
	int A[] = {10, 20, 30, 40, 50};
	int B[] = {5, 15, 25, 35, 45};

	create(A, 5);
	create2(B, 5);

	printf("First\n");
	display(first);

	t1 = first->next->next;
	t2 = first->next->next->next->next;
	t2->next = t1;

	if (isLoop(first)) {
		printf("First List Contains Loop\n");
	} else {
		printf("First List does not contains Loop\n");
	}
	
	printf("Second\n");
	display(second);
	
	if (isLoop(second)) {
		printf("Second List Contains Loop\n");
	} else {
		printf("Second List does not contains Loop\n");
	}

	return 0;
}
