#include <stdio.h>
#include <stdlib.h>

// Program to concatenate 2 linked list

// Self referencial structure
struct Node
{
	int data;
	struct Node *next;
}*first = NULL, *second = NULL, *third = NULL;

void create(int A[], int n)
{
	int i;
	struct Node *t, *last;

	first = (struct Node *)malloc(sizeof(struct Node));
	first->data = A[0];
	first->next = NULL;
	last = first;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = NULL;
		last->next = t;
		last = t;
	}
}

void create2(int A[], int n)
{
	int i;
	struct Node *t, *last;

	second = (struct Node *)malloc(sizeof(struct Node));
	second->data = A[0];
	second->next = NULL;
	last = second;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = NULL;
		last->next = t;
		last = t;
	}
}

void display(struct Node *p)
{
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

void Concat(struct Node *p, struct Node *q)
{
	third = p;

	while (p->next != NULL)
	{
		p = p->next;
	}
	p->next = q;
}

int main()
{
	int A[] = {10, 20, 30, 40, 50};
	int B[] = {5, 15, 25, 35, 45};

	create(A, 5);
	create2(B, 5);

	printf("First\n");
	display(first);
	printf("\n\n");
	
	printf("Second\n");
	display(second);
	printf("\n\n");

	Concat(first, second);
	printf("Concatenated\n");
	display(third);
	printf("\n\n");

	return 0;
}
