#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

// Program to create linked list using InsertLast()
// Function to check whether linked list is sorted or not
// Function to remove duplicates from sorted linked list

// Self referencial structure
struct Node
{
	int data;
	struct Node *next;
}*first = NULL;

struct Node *last = NULL;

void InsertLast(int x)
{
	struct Node *t;

	t = (struct Node *)malloc(sizeof(struct Node));
	t->data = x;
	t->next = NULL;

	if (first == NULL) {
		first = last = t;
	} else {
		last->next = t;
		last = t;
	}
}

void display(struct Node *p)
{
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

int isSorted(struct Node *p)
{
	int x = INT_MIN;

	while (p != NULL) {
		if (p->data < x) {
			return 0;
		}
		x = p->data;
		p = p->next;
	}
	return 1;
}

void RemoveDuplicate(struct Node *p)
{
	struct Node *q = p->next;

	while (q != NULL) {
		if (p->data != q->data) {
			p = q;
			q = q->next;
		} else {
			p->next = q->next;
			free(q);
			q = p->next;
		}
	}
}

int main()
{
	InsertLast(10);
	display(first);
	InsertLast(10);
	display(first);
	InsertLast(20);
	display(first);
	InsertLast(40);
	display(first);
	InsertLast(40);
	display(first);
	InsertLast(50);
	display(first);

	if (isSorted(first)) {
		printf("Sorted\n");
	} else {
		printf("Not sorted\n");
	}

	RemoveDuplicate(first);
	display(first);

	return 0;
}
