#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

// Program to reverse the linked list

/* Methods
 * 1. Reversing elements: means data gets swaped in nodes
 * 2. Reversing links(Preferred): means node contains same data links are reversed
 */

// Self referencial structure
struct Node
{
	int data;
	struct Node *next;
}*first = NULL;

void create(int A[], int n)
{
	int i;
	struct Node *t, *last;

	first = (struct Node *)malloc(sizeof(struct Node));
	first->data = A[0];
	first->next = NULL;
	last = first;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = NULL;
		last->next = t;
		last = t;
	}
}

void display(struct Node *p)
{
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

void SortedInsert(struct Node *p, int x)
{
	struct Node *t, *q = NULL;

	t = (struct Node *)malloc(sizeof(struct Node));
	t->data = x;
	t->next = NULL;

	if (first == NULL) {
		first = t;
	} else {
		while (p && p->data < x) {
			q = p; // previous node
			p = p->next;
		}
		if (p == first) {
			t->next = first;
			first = t;
		} else {
			t->next = q->next;
			q->next = t;
		}
	}
}

int count(struct Node *p)
{
	int l = 0;

	while (p != NULL) {
		l++;
		p = p->next;
	}

	return l;
}

// Reversing elements
void Reverse1(struct Node *p)
{
	int *A, i = 0;
	struct Node *q = p;

	A = (int *)malloc(sizeof(int) * count(p));

	while (q != NULL) {
		A[i] = q->data;
		q = q->next;
		i++;
	}
	q = p;
	i--;
	
	while (q != NULL) {
		q->data = A[i];
		q = q->next;
		i--;
	}
}

// Reversing links
void Reverse2(struct Node *p)
{
	struct Node *q = NULL, *r = NULL;

	while (p != NULL) {
		r = q;
		q = p;
		p = p->next;
		q->next = r;
	}
	first = q;
}

void Reverse3(struct Node*q, struct Node *p)
{
	if (p != NULL) {
		Reverse3(p, p->next);
		p->next = q;
	} else {
		first = q;
	}
}

int main()
{
	struct Node *temp;
	int A[] = {10, 20, 30, 40, 50};

	create(A, 5);
	printf("Insert 35\n");
	SortedInsert(first, 35);
	display(first);
	printf("Insert 5\n");
	SortedInsert(first, 5);
	display(first);
	printf("Insert 60\n");
	SortedInsert(first, 60);
	display(first);

	printf("Reverse using Reversing elements\n");
	Reverse1(first);
	display(first);
	
	printf("Reverse using Reversing links\n");
	Reverse2(first);
	display(first);
	
	printf("Reverse using Reversing links Recursively\n");
	Reverse3(NULL, first);
	display(first);
	return 0;
}
