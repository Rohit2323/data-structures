#include <stdio.h>
#include <stdlib.h>

// Program to check whether 2 linked list are intersected or not

// Self referencial structure
struct Node
{
	int data;
	struct Node *next;
}*first = NULL, *second = NULL;

void create(int A[], int n)
{
	int i;
	struct Node *t, *last;

	first = (struct Node *)malloc(sizeof(struct Node));
	first->data = A[0];
	first->next = NULL;
	last = first;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = NULL;
		last->next = t;
		last = t;
	}
}

void create2(int A[], int n)
{
	int i;
	struct Node *t, *last;

	second = (struct Node *)malloc(sizeof(struct Node));
	second->data = A[0];
	second->next = NULL;
	last = second;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = NULL;
		last->next = t;
		last = t;
	}
}

void display(struct Node *p)
{
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

int Length(struct Node *p)
{
	int len = 0;

	while (p != NULL) {
		len++;
		p = p->next;
	}
	return len;
}

// Method 1: Using length checks
// Time complexity: O(n)
void CheckIntersection(struct Node *p, struct Node *q)
{
	int isFound = 0;
	int lenFirst = 0, lenSecond = 0;
	int lenDiff = 0;

	lenFirst = Length(first);
	lenSecond = Length(second);
 
	if (lenFirst > lenSecond) {
		lenDiff = lenFirst - lenSecond;

		while (lenDiff != 0) {
			p = p->next;
			lenDiff--;
		}
	} else {
		lenDiff = lenSecond - lenFirst;

		while (lenDiff != 0) {
			q = q->next;
			lenDiff--;
		}
	}

	while (p && q) {
		if (p == q) {
			printf("Method1: Intersection found at %d\n", p->data);
			isFound = 1;
			break;
		} else {
			p = p->next;
			q = q->next;
		}
	}

	if (isFound == 0) {
		printf("Method1: Intersection not found\n");
	}
}

struct Stack
{
	int size;
	int top;
	struct Node *S[10];
};

void createStack(struct Stack *st)
{
	st->top = -1;
	//st->S = (struct Node *)malloc(10 * sizeof(struct Node));
}

void DisplayStack(struct Stack st)
{
	int i;

	for (i = st.top; i >= 0; i--) {
		printf("%d ", st.S[i]->data);
	}
	printf("\n\n");
}

void push(struct Stack *st, struct Node *p)
{
	if (st->top == st->size - 1) {
		printf("Stack Overflow\n");
	} else {
		st->top++;
		st->S[st->top] = p;
	}
}

struct Node * pop(struct Stack *st)
{
	struct Node *t = NULL;

	if (st->top == -1) {
		printf("Stack Underflow\n");
	} else {
		t = st->S[st->top--];
	}
	return t;
}

int isEmpty(struct Stack st)
{
	if (st.top == -1) {
		return 1;
	}
	return 0;
}

int isFull(struct Stack st)
{
	return st.top == st.size - 1;
}

struct Node * stackTop(struct Stack st)
{
	if (!isEmpty(st)) {
		return st.S[st.top];
	}
	return NULL;
}

// Method 2: Using stack
// Time complexity: O(n)
void CheckIntersection2(struct Node *p, struct Node *q)
{
	int isFound = 0;
	struct Stack st1, st2;
	struct Node *t = NULL;

	createStack(&st1);
	createStack(&st2);

	while (p) {
		push(&st1, p);
		p = p->next;
	}
	printf("Stack st1\n");
	DisplayStack(st1);
	
	while (q) {
		push(&st2, q);
		q = q->next;
	}
	printf("Stack st2\n");
	DisplayStack(st2);

	while (stackTop(st1) == stackTop(st2)) {
		t = pop(&st1);
		pop(&st2);
	}
	if (t == NULL) {
		printf("Method2: Intersection not found\n");
	} else {
		printf("Method2: Intersection found at %d\n", t->data);
	}
}

int main()
{
	struct Node *t1, *t2;
	int A[] = {10, 20, 30, 40, 50};
	int B[] = {5, 15};

	create(A, 5);
	create2(B, 2);

	printf("First\n");
	display(first);
	
	printf("Second\n");
	display(second);
	
	CheckIntersection(first, second);
	CheckIntersection2(first, second);
	
	t1 = first->next->next;
	t2 = second->next;
	t2->next = t1;

	printf("Second\n");
	display(second);

	CheckIntersection(first, second);
	CheckIntersection2(first, second);
	
	return 0;
}
