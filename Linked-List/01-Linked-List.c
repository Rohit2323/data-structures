#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

// Program to create linked list and display it

// Self referencial structure
struct Node
{
	int data;
	struct Node *next;
}*first = NULL;

void create(int A[], int n)
{
	int i;
	struct Node *t, *last;

	first = (struct Node *)malloc(sizeof(struct Node));
	first->data = A[0];
	first->next = NULL;
	last = first;

	for (i = 1; i < n; i++) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = A[i];
		t->next = NULL;
		last->next = t;
		last = t;
	}
}

void display(struct Node *p)
{
	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n");
}

void Rdisplay(struct Node *p)
{
	if (p != NULL) {
		printf("%d ", p->data);
		Rdisplay(p->next);
	}
}

// Time complexity: O(n)
// Space complexity: O(1)
int count(struct Node *p)
{
	int l = 0;

	while (p != NULL) {
		l++;
		p = p->next;
	}

	return l;
}

// Time complexity: O(n)
// Space complexity: O(n)
int Rcount(struct Node *p)
{
	if (p != NULL) {
		return Rcount(p->next) + 1;
	} else {
		return 0;
	}
}

int sum(struct Node *p)
{
	int s = 0;

	while (p != NULL) {
		s += p->data;
		p = p->next;
	}

	return s;
}

int Rsum(struct Node *p)
{
	if (p == NULL) {
		return 0;
	} else {
		return Rsum(p->next) + p->data;
	}
}

int max(struct Node *p)
{
	int max = INT_MIN;

	while (p != NULL) {
		if (p->data > max) {
			max = p->data;
		}
		p = p->next;
	}

	return max;
}

int Rmax(struct Node *p)
{
	int x = 0;

	if (p == NULL) {
		return 0;
	}

	x = Rmax(p->next);
	return x > p->data? x : p->data;
}

int min(struct Node *p)
{
	int min = INT_MAX;

	while (p != NULL) {
		if (p->data < min) {
			min = p->data;
		}
		p = p->next;
	}

	return min;
}

int Rmin(struct Node *p)
{
	int x = 0;

	if (p == NULL) {
		return 0;
	}

	x = Rmax(p->next);
	return x < p->data? x : p->data;
}

struct Node * Linear_Search(struct Node *p, int key)
{
	while (p != NULL) {
		if (key == p->data) {
			return p;
		}
		p = p->next;
	}

	return NULL;
}

struct Node * RLinear_Search(struct Node *p, int key)
{
	if (p == NULL) {
		return NULL;
	}

	if (key == p->data) {
		return p;
	}
	RLinear_Search(p->next, key);
}

// Linear search with improvement using Mode-At-Head so that when same key searched it is searched in O(1) time
struct Node * ILinear_Search(struct Node *p, int key)
{
	struct Node*q;

	while (p != NULL) {
		if (key == p->data) {
			q->next = p->next;
			p->next = first;
			first = p;
			return p;
		}
		q = p; // previous node
		p = p->next;
	}

	return NULL;
}

void Insert(struct Node *p, int index, int x)
{
	struct Node *t;
	int i;

	if (index < 0 || index > count(p)) {
		return;
	}

	t = (struct Node *)malloc(sizeof(struct Node));
	t->data = x;

	// Add First
	if (index == 0) { // O(1)
		t->next = first;
		first = t;
	} else {
		for (i = 0; i < index - 1; i++) {
			p = p->next;
		}
		t->next = p->next;
		p->next = t;
	}
}

int Delete(struct Node *p, int index)
{
	struct Node *q = NULL;
	int x = -1, i;

	if (index < 0 || index > count(p)) {
		return -1;
	}

	if (index == 1) {
		p = first;
		x = first->data;
		first = first->next;
		free(p);
		return x;
	} else {
		for (i = 0; i < index - 1; i++) {
			q = p;
			p = p->next;
		}
		q->next = p->next;
		x = p->data;
		free(p);
		return x;
	}
}

int middle_node_of_list(struct Node *p)
{
	struct Node *q = p;

	while (q != NULL) {
		q = q->next;
		if (q) {
			q = q->next;
		}
		if (q) {
			p = p->next;
		}
	}
	return p->data;
}

int main()
{
	struct Node *temp;
	int A[] = {3, 5, 7, 10, 15, 8, 12, 20};

	create(A, 8);
	printf("Display using interative\n");
	display(first);
	printf("Display using recursion\n");
	Rdisplay(first);
	printf("\n");
	printf("Length using iterative: %d\n", count(first));
	printf("Length using recursion: %d\n", Rcount(first));
	printf("Sum using iterative: %d\n", sum(first));
	printf("Sum using recursion: %d\n", Rsum(first));
	printf("Max using iterative: %d\n", max(first));
	printf("Max using recursion: %d\n", Rmax(first));
	printf("Min using iterative: %d\n", min(first));
	printf("Min using recursion: %d\n", Rmin(first));

	printf("Search using linear search\n");
	temp = Linear_Search(first, 20);
	if (temp) {
		printf("Key is Found %d\n", temp->data);
	} else {
		printf("Key is not found\n");
	}

	printf("Search using recursive linear search\n");
	temp = RLinear_Search(first, 25);
	if (temp) {
		printf("Key is Found %d\n", temp->data);
	} else {
		printf("Key is not found\n");
	}
	
	printf("Search using improved linear search using Move-At-Head\n");
	temp = ILinear_Search(first, 20);
	temp = ILinear_Search(first, 12);
	if (temp) {
		printf("Key is Found %d\n", temp->data);
	} else {
		printf("Key is not found\n");
	}
	display(first);

	printf("Insert 1 at index 0\n");
	Insert(first, 0, 1);
	display(first);
	printf("Insert 11 at index 3\n");
	Insert(first, 3, 11);
	display(first);
	printf("Insert 13 at index 13\n");
	Insert(first, 13, 13);
	display(first);
	printf("Insert 30 at index 10\n");
	Insert(first, 10, 30);
	display(first);

	printf("Deleted Element %d\n", Delete(first, 10));
	display(first);
	printf("Deleted Element %d\n", Delete(first, 4));
	display(first);
	printf("Middle node of list : %d\n", middle_node_of_list(first));
	printf("Deleted Element %d\n", Delete(first, 1));
	display(first);

	printf("Middle node of list : %d\n", middle_node_of_list(first));

	return 0;
}
