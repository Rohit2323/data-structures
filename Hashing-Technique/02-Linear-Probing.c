#include <stdio.h>

// Program to implement hashing based on linear probing
/*
	a. Closed Hashing
		i. Linear probing:
		In this we use only Hash table space. No extra space
		If collision is happened during insertion then search for next blank space and store there.
*/

#define SIZE 10

int hash(int key)
{
	return key%10;
}

int probe(int H[], int key)
{
	int index = hash(key);
	int i = 0;

	while (H[(index+i)%SIZE] != 0) {
		i++;
	}

	return (index+i)%SIZE;
}

void Insert(int H[], int key)
{
	int index = hash(key);

	if (H[index] != 0) {
		index = probe(H, key);
	}
	H[index] = key;
}

int Search(int H[], int key)
{
	int index = hash(key);
	int i = 0;

	while (H[(index+i)%SIZE] != key) {
		i++;
	}

	return (index+i)%SIZE;
}

int main()
{
	int HT[SIZE] = {0};

	Insert(HT, 12);
	Insert(HT, 25);
	Insert(HT, 35);
	Insert(HT, 26);

	int key = 35;
	printf("Key %d found at %d\n", key, Search(HT, key));

	return 0;
}
