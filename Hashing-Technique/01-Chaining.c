#include <stdio.h>
#include <stdlib.h>

// Program to implement hashing based on chaining
/*
	a. Open Hashing
		i. Chaining: h(x) = x % 10;
		n is no of keys, size is hash table size
		Loading factor lamba  = n/size
		Time Avg successful search t = 1 + (lamba/2)
		Time Avg unsuccessful search t = 1 + (lamba)
*/

/****************************** Linked List *********************************/
struct Node
{
	int data;
	struct Node *next;
};

void SortedInsert(struct Node **H, int x)
{
	struct Node *t, *q = NULL, *p = *H;

	t = (struct Node *)malloc(sizeof(struct Node));
	t->data = x;
	t->next = NULL;

	if (*H == NULL) {
		*H = t;
	} else {
		while (p && p->data < x) {
			q = p; // previous node
			p = p->next;
		}
		if (p == *H) {
			t->next = *H;
			*H = t;
		} else {
			t->next = q->next;
			q->next = t;
		}
	}
}

struct Node * Search(struct Node *p, int key)
{
	while (p != NULL) {
		if (key == p->data) {
			return p;
		}
		p = p->next;
	}

	return NULL;
}

/****************************** Hash *********************************/
int hash(int key)
{
	return key%10;
}
void Insert(struct Node *H[], int key)
{
	int index = hash(key);

	SortedInsert(&H[index], key);
}

int main()
{
	struct Node *HT[10];
	struct Node *temp;
	int i, key;

	for (i = 0; i < 10; i++) {
		HT[i] = NULL;
	}

	Insert(HT, 12);
	Insert(HT, 22);
	Insert(HT, 42);

	key = 22;
	temp = Search(HT[hash(key)], key);
	if (temp) {
		printf("%d Element found\n", temp->data);
	} else {
		printf("%d Element not found\n", key);
	}
	
	key = 52;
	temp = Search(HT[hash(key)], key);
	if (temp) {
		printf("%d Element found\n", temp->data);
	} else {
		printf("%d Element not found\n", key);
	}
	return 0;
}
