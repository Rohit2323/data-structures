	a. Open Hashing
		i. Chaining: h(x) = x % 10;
		n is no of keys, size is hash table size
		Loading factor lamba  = n/size
		Time Avg successful search t = 1 + (lamba/2)
		Time Avg unsuccessful search t = 1 + (lamba)

	b. Closed Hashing
		i. Linear probing:
		In this we use only Hash table space. No extra space
		If collision is happened during insertion then search for next blank space and store there.

		h(x) = x % size;
		h`(x) = (h(x) + f(i));  f(i) = i where i = 0, 1, 2
		Loading factor should not be more than 0.5

		ii. Quadratic Probing:
		h(x) = x % size;
		h`(x) = (h(x) + f(i));  f(i) = i^2 where i = 0, 1, 2
		Loading factor should not be more than 0.5

		iii. Double hashing:
		h1(x) = x % size;
		h2(x) = R - (x%R) where R is primary number smaller than size of hash table eg. if size is 10 then R = 7
 		h`(x) = (h1(x) + i*h2(x));  where i = 0, 1, 2
