#include <stdio.h>
#include <math.h>

// Program to implement hashing based on quadratic probing
/*
	a. Closed Hashing
	ii. Quadratic Probing:
		h(x) = x % size;
		h`(x) = (h(x) + f(i));  f(i) = i^2 where i = 0, 1, 2
		Loading factor should not be more than 0.5
*/

#define SIZE 10

int hash(int key)
{
	return key%10;
}

int probe(int H[], int key)
{
	int index = hash(key);
	int i = 0;

	while (H[(index+(int)pow(i,2))%SIZE] != 0) {
		i++;
	}

	return (index+(int)pow(i,2))%SIZE;
}

void Insert(int H[], int key)
{
	int index = hash(key);

	if (H[index] != 0) {
		index = probe(H, key);
	}
	H[index] = key;
}

int Search(int H[], int key)
{
	int index = hash(key);
	int i = 0;

	while (H[(index+(int)pow(i,2))%SIZE] != key) {
		i++;
	}

	return (index+(int)pow(i,2))%SIZE;
}

int main()
{
	int HT[SIZE] = {0};

	Insert(HT, 12);
	Insert(HT, 25);
	Insert(HT, 35);
	Insert(HT, 45);
	Insert(HT, 26);

	int key = 45;
	printf("Key %d found at %d\n", key, Search(HT, key));

	return 0;
}
