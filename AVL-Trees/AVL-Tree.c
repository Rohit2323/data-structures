#include <stdio.h>
#include <stdlib.h>

// Program to implement AVL tree

/*
	 AVL tree - Is nothing but height balanced binary search tree
	Balance factor = Height of left sub-tree - Height of right sub-tree
	BF = hl - hr	where allowed bf = {-1, 0, 1}
	Use of following rotations:
	a. LL 
	b. LR
	c. RR
	d. RL

	Note: Rotation can be perfomed on Node which is imbalance. 3 nodes are involved in it.
*/

struct Node
{
	struct Node *lchild;
	int data;
	int height;
	struct Node *rchild;
}*root = NULL;

int NodeHeight(struct Node *p)
{
	int hl, hr;

	hl = p && p->lchild ? p->lchild->height : 0;
	hr = p && p->rchild ? p->rchild->height : 0;

	return hl > hr ? hl+1 : hr+1;
}

int BalanceFactor(struct Node *p)
{
	int hl, hr;

	hl = p && p->lchild ? p->lchild->height : 0;
	hr = p && p->rchild ? p->rchild->height : 0;

	return hl-hr;
}

struct Node *LLRotation(struct Node *p)
{
	struct Node *pl = p->lchild;
	struct Node *plr = pl->rchild;

	pl->rchild = p;
	p->lchild = plr;

	// Update the height of modified nodes
	p->height = NodeHeight(p);
	pl->height = NodeHeight(pl);

	if (root == p) {
		root = pl;
	}

	return pl;
}

struct Node *LRRotation(struct Node *p)
{
	struct Node *pl = p->lchild;
	struct Node *plr = pl->rchild;

	pl->rchild = plr->lchild;
	p->lchild = plr->rchild;
	plr->lchild = pl;
	plr->rchild = p;
	
	pl->height = NodeHeight(pl);
	p->height = NodeHeight(p);
	plr->height = NodeHeight(plr);

	if (root == p) {
		root = plr;
	}

	return plr;
}

struct Node *RRRotation(struct Node *p)
{
	struct Node *pr = p->rchild;
	struct Node *prl = pr->lchild;

	pr->lchild = p;
	p->rchild = prl;

	// Update the height of modified nodes
	p->height = NodeHeight(p);
	pr->height = NodeHeight(pr);

	if (root == p) {
		root = pr;
	}

	return pr;
}

struct Node *RLRotation(struct Node *p)
{
	struct Node *pr = p->rchild;
	struct Node *prl = pr->lchild;

	pr->lchild = prl->rchild;
	p->rchild = prl->lchild;
	prl->rchild = pr;
	prl->lchild = p;
	
	pr->height = NodeHeight(pr);
	p->height = NodeHeight(p);
	prl->height = NodeHeight(prl);

	if (root == p) {
		root = prl;
	}

	return prl;
}

struct Node *Insert(struct Node *p, int key)
{
	struct Node *t = NULL;

	if (p == NULL) {
		t = (struct Node *)malloc(sizeof(struct Node));
		t->data = key;
		t->height = 1;
		t->lchild = t->rchild = NULL;
		return t;
	}

	if (key == p->data) {
		return NULL;
	} else if (key < p->data) {
		// Assignment to pointer is important
		p->lchild = Insert(p->lchild, key);
	} else if (key > p->data) {
		p->rchild = Insert(p->rchild, key);
	}

	// Update node height
	p->height = NodeHeight(p);

	if (BalanceFactor(p) == 2 && BalanceFactor(p->lchild) == 1) {
		return LLRotation(p);
	} else if (BalanceFactor(p) == 2 && BalanceFactor(p->lchild) == -1) {
		return LRRotation(p);
	} else if (BalanceFactor(p) == -2 && BalanceFactor(p->rchild) == -1) {
		return RRRotation(p);
	} else if (BalanceFactor(p) == -2 && BalanceFactor(p->rchild) == 1) {
		return RLRotation(p);
	}

	return p;
}

void Inorder(struct Node *p)
{
	if (p) {
		Inorder(p->lchild);
		printf("%d ", p->data);
		Inorder(p->rchild);
	}
}

int main()
{
	// Test LL Rotation
	/*root = Insert(root, 10);
	Insert(root, 5);
	Insert(root, 2);*/
	
	// Test LR Rotation
	/*root = Insert(root, 50);
	Insert(root, 10);
	Insert(root, 20);*/
	
	// Test RR Rotation
	/*root = Insert(root, 10);
	Insert(root, 20);
	Insert(root, 30);*/
	
	// Test RL Rotation
	/*root = Insert(root, 10);
	Insert(root, 30);
	Insert(root, 20);*/
	
	root = Insert(root, 10);
	Insert(root, 20);
	Insert(root, 30);
	Insert(root, 25);
	Insert(root, 28);
	Insert(root, 27);
	Insert(root, 5);

	Inorder(root);
	printf("\n");

	return 0;
}
