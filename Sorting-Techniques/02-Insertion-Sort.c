#include <stdio.h>

// Program to implement insertion sort

/*
	a. No of pass: n-1
	b. No of comparisons : n*(n-1)/2 = O(n^2)
	c. No of Swaps:  n*(n-1)/2 = O(n^2)
	d. Adaptive: By nature only
	e. Stable: Yes
	f. Time complexity:
	Min: O(n)			Swap: O(1)
	Max: O(n^2) 	Swap: O(n^2)	Example : If array in descending in order
	g. Mostly suitable in Linked list as not shifting is required
*/


void InsertionSort(int A[], int n)
{
	int i, j, x;

	for (i = 1; i < n; i++) { // Passes
		j = i-1;
		x = A[i];

		while (j > -1 && A[j] > x) {
			A[j+1] = A[j];
			j--;
		}
		A[j+1] = x;
	}
}

int main()
{
	int A[] = {3, 7, 9, 10, 6, 5, 12, 4, 11, 2};
	//int A[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // for Adaptive test case
	int n = 10, i;

	InsertionSort(A, n);

	for (i = 0; i < n; i++)	{
		printf("%d ", A[i]);
	}
	printf("\n");

	return 0;
}
