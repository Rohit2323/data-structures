#include <stdio.h>
#include <stdlib.h>

// Program to implement Shell sort

/*
	a. Extension of Insertion sort.
	b. No of pass: log n
	c. Time complexity: O(nlogn) or O(n^(3/2))
	d. Space complexity: In place replacement so no extra space
	e. Used for sorting very large list
*/

void ShellSort(int A[], int n)
{
	int gap, i, j, temp;

	for (gap = n/2; gap >= 1; gap/=2) { // passes
		for (i = gap; i < n; i++) {
			temp = A[i];
			j = i-gap;

			while ((j >= 0) && (A[j] > temp)) {
				A[j+gap] = A[j];
				j = j-gap;
			}
			A[j+gap] = temp;
		}
	}
}

int main()
{
	int A[] = {11, 13, 7, 12, 16, 9, 24, 5, 10, 3};
	//int A[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // for Adaptive test case
	int n = 10, i;

	ShellSort(A, n);

	for (i = 0; i < n; i++)	{
		printf("%d ", A[i]);
	}
	printf("\n");

	return 0;
}
