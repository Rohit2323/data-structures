#include <stdio.h>

// Program to implement bubble sort

/*
	a. No of pass: n-1
	b. No of comparisons : n*(n-1)/2 = O(n^2)
	c. No of Swaps:  n*(n-1)/2 = O(n^2)
	d. Adaptive: Using extra flag
	e. Stable: Yes
	f. Time complexity:
	Min: O(n)
	Max: O(n^2)
	g. k passes will give us k largest sorted element
*/

void swap(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

void BubbleSort(int A[], int n)
{
	int i, j, flag = 0;

	for (i = 0; i < n-1; i++) { // Passes
		flag = 0;
		for (j = 0; j < n-1-i; j++) {
			if (A[j] > A[j+1]) {
				swap(&A[j], &A[j+1]);
				flag = 1;
			}
		}
		if (flag == 0) {
			break; // No swaping done means array already sorted
		}
	}
}

int main()
{
	int A[] = {3, 7, 9, 10, 6, 5, 12, 4, 11, 2};
	//int A[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // for Adaptive test case
	int n = 10, i;

	BubbleSort(A, n);

	for (i = 0; i < n; i++)	{
		printf("%d ", A[i]);
	}
	printf("\n");

	return 0;
}
