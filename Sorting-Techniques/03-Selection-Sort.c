#include <stdio.h>

// Program to implement selection sort

/*
	a. No of pass: n-1
	b. No of comparisons : n*(n-1)/2 = O(n^2)
	c. No of Swaps:  1 = O(1)
	d. Adaptive: No
	e. Stable: No
	f. Time complexity:
	O(n^2) 	Swap: O(1)
	g. k passes will give us k smallest sorted element
	h. It perform minimum no of swaps
*/

void swap(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

void SelectionSort(int A[], int n)
{
	int i, j, k;

	for (i = 0; i < n-1; i++) { // Passes
		for (j = k = i; j < n; j++) {
			if (A[j] < A[k]) {
				k = j;
			}
		}
		swap(&A[i], &A[k]);
	}
}

int main()
{
	int A[] = {3, 7, 9, 10, 6, 5, 12, 4, 11, 2};
	//int A[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // for Adaptive test case
	int n = 10, i;

	SelectionSort(A, n);

	for (i = 0; i < n; i++)	{
		printf("%d ", A[i]);
	}
	printf("\n");

	return 0;
}
