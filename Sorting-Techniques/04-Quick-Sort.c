#include <stdio.h>
#include <limits.h>

// Program to implement quick sort

/*
	a. Adaptive: No
	b. Stable: No
	c. Time complexity:
	Best case : O(nlogn) - if partitioning is in middle
	Worst case : O(n^2) - is partitioning is on any end
*/

void swap(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

int partition(int A[], int l, int h)
{
	int pivot = A[l];
	int i = l, j = h;

	do {
		do { i++; } while (A[i] <= pivot);
		do { j--; } while (A[j] > pivot);

		if (i < j) swap(&A[i], &A[j]);
	} while (i < j);
	swap(&A[l], &A[j]);

	return j;
}

void QuickSort(int A[], int l, int h)
{
	int j;

	if (l < h) {
		j = partition(A, l, h);
		QuickSort(A, l, j); // j th element is sorted element activing as infinity
		QuickSort(A, j+1, h); // hth element is infinity
	}
}

int main()
{
	int A[] = {11, 13, 7, 12, 16, 9, 24, 5, 10, 3, INT_MAX};
	//int A[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // for Adaptive test case
	int n = 11, i;

	QuickSort(A, 0, 10);

	for (i = 0; i < n-1; i++)	{
		printf("%d ", A[i]);
	}
	printf("\n");

	return 0;
}
