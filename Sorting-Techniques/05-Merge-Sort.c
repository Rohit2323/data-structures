#include <stdio.h>

// Program to implement merge sort using iterative and recursive method

/*
	a. No of pass: log n
	a. Adaptive: No
	b. Stable: Yes
	c. Time complexity:
	O(nlogn)
	d. Space complexity:
	2n+logn i.e n+logn extra space is required
				l			  mid|j				h
	int A[] = {11, 13, 7, 12, 16, 9, 24, 5, 10, 3};
	Assume that single array contains two list one from l - mid and j - h

	Also called as 2-way merge sort
*/

void swap(int *x, int *y)
{
	int temp = *x;
	*x = *y;
	*y = temp;
}

void Merge(int A[], int l, int mid, int h)
{
	int i = l, j = mid+1, k = l;
	int B[100];

	while (i <= mid && j <= h) {
		if (A[i] < A[j]) {
			B[k++] = A[i++];
		} else {
			B[k++] = A[j++];
		}
	}
	for (; i <= mid; i++) {
		B[k++] = A[i];
	}
	for (; j <= h; j++) {
		B[k++] = A[j];
	}

	for (int i = l; i <= h; i++) {
		A[i] = B[i];
	}
}

void IMergeSort(int A[], int n)
{
	int p, l, h, mid, i;

	for (p = 2; p <= n; p = p*2) { // passes
		for (i = 0; i+p-1 < n; i = i+p) {
			l = i;
			h = i+p-1;
			mid = (l+h)/2;
			Merge(A, l, mid, h);
		}
	}
	// If array contains odd numbers
	if (p/2 < n) {
		Merge(A, 0, p/2-1, n);
	}
}

void RMergeSort(int A[], int l, int h)
{
	int mid;

	if (l < h) {
		mid = (l+h)/2;
		RMergeSort(A, l, mid);
		RMergeSort(A, mid+1, h);
		Merge(A, l, mid, h);
	}
}

int main()
{
	int A[] = {11, 13, 7, 12, 16, 9, 24, 5, 10, 3};
	//int A[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}; // for Adaptive test case
	int n = 10, i;

	//IMergeSort(A, n);
	RMergeSort(A, 0, 9);

	for (i = 0; i < n; i++)	{
		printf("%d ", A[i]);
	}
	printf("\n");

	return 0;
}
