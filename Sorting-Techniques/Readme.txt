* Criteria for Analysis
	a. Number of comparisons
	b. Number of Swaps
	c. Adaptive: If element are already sorted then time should be less or minimum.
	d. Stable: If elements are duplicate then during comparison maintain then their order in which there are already present(Example: In Database column sort)
	e. Extra Memory
	
	* Types
	1. Comparison Based Sort
			--------------------
			|	1. Bubble
	O(n^2)	|	2. Insertion
			|	3. Selection
			--------------------
			--------------------
			|	4. Heap
	O(nlogn)|	5. Merge
	less	|	6. Quick
	space	|	7. Tree
			--------------------
			--------------------
	O(n^3/2)	|	8. Shell
			--------------------
			
	2. Index Based Sort
			--------------------
	O(n)	|	9. Count
	more	|	10. Bucket/Bin
	space 	|	11. Radix
			--------------------

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
1. Bubble Sort
	a. No of pass: n-1
	b. No of comparisons : n*(n-1)/2 = O(n^2)
	c. No of Swaps:  n*(n-1)/2 = O(n^2)
	d. Adaptive: Using extra flag
	e. Stable: Yes
	f. Time complexity:
	Min: O(n)
	Max: O(n^2)
	g. k passes will give us k largest sorted element

2. Insertion Sort
	a. No of pass: n-1
	b. No of comparisons : n*(n-1)/2 = O(n^2)
	c. No of Swaps:  n*(n-1)/2 = O(n^2)
	d. Adaptive: By nature only
	e. Stable: Yes
	f. Time complexity:
	Min: O(n)			Swap: O(1)
	Max: O(n^2) 	Swap: O(n^2)	Example : If array in descending in order

3. Selection Sort
	a. No of pass: n-1
	b. No of comparisons : n*(n-1)/2 = O(n^2)
	c. No of Swaps:  1 = O(1)
	d. Adaptive: No
	e. Stable: No
	f. Time complexity:
	O(n^2) 	Swap: O(1)
	g. k passes will give us k smallest sorted element
	h. It perform minimum no of swaps

4. Quick Sort
	a. Adaptive: No
	b. Stable: No
	c. Time complexity:
	Best case : O(nlogn) - if partitioning is in middle
	Worst case : O(n^2) - is partitioning is on any end

5. Merge Sort
	a. No of pass: log n
	a. Adaptive: No
	b. Stable: Yes
	c. Time complexity:
	O(nlogn)
	d. Space complexity:
	2n+logn i.e n+logn extra space is required
               		    l             mid|j                       h
 	int A[] = {11, 13, 7, 12, 16, 9, 24, 5, 10, 3};
 	Assume that single array contains two

6. Count Sort
	a. Time complexity: O(n)
	b. Space complexity: Requires more space

7. Bucket/Bin Sort
	a. Similar to Count sort instead of count taken lists
	b. Time complexity: O(n)
	c. Space complexity: Requires more space

8. Radix Sort
	a. Similar to Bucket sort. Auxiliary array is of size based on number system. For decimal number size is 10 i.e 0-9
	b. No of pass: Number of digits in max number in an array if 1112 then pass = 4 if 222222 pass = 6
	c. Time complexity: O(n)
	d. Space complexity: Requires less space than that of count/bucket sort

9. Shell Sort
	a. Extension of Insertion sort.
	b. No of pass: log n
	c. Time complexity: O(nlogn) or O(n^(3/2))
	d. Space complexity: In place replacement so no extra space
	e. Used for sorting very large list
