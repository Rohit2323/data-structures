#include <stdio.h>
#include <stdlib.h>

// Program to check parenthesis matching

// ((a+b)*(c*d)) = matching
// (((a+b)*(c*d)) = not matching
// ((a+b)*(c*d))) = not matching

struct Node
{
	char data;
	struct Node *next;
}*top = NULL;

void push(char x)
{
	struct Node *t;
	
	t = (struct Node *)malloc(sizeof(struct Node));
	if (t == NULL) {
		printf("Stack is Full\n");
	} else {
		t->data = x;
		t->next = top;
		top = t;
	}

}

char pop()
{
	struct Node *t;
	char x = -1;

	if (top == NULL) {
		printf("Stack is Empty\n");
	} else {
		t = top;
		top = top->next;
		x = t->data;
		free(t);
	}
	return x;
}

int isEmpty()
{
	return top? 0 : 1;
}

// ((a+b)*(c*d)) = matching
// (((a+b)*(c*d)) = not matching
// ((a+b)*(c*d))) = not matching
int isBalanced(char *exp)
{
	int i;

	for (i = 0; i < exp[i] != '\0'; i++) {
		if (exp[i] == '(') {
			push(exp[i]);
		} else if (exp[i] == ')') {
			if (isEmpty()) {
				return 0;
			}
			pop();
		}
	}
	if (isEmpty()) {
		return 1;
	} else {
		return 0;
	}
}

// {([a+b]*[c-d])/e} = matching
// {([a+b)*[c-d])/e} = not matching
int isBalanced2(char *exp)
{
	int i;
	char x;

	for (i = 0; i < exp[i] != '\0'; i++) {
		if (exp[i] == '{' || exp[i] == '(' || exp[i] == '[') {
			push(exp[i]);
		} else if (exp[i] == '}' || exp[i] == ')' || exp[i] == ']') {
			if (isEmpty()) {
				return 0;
			}
			x = pop();
			if ((exp[i] == '}') && (x != '{')) {
				return 0;
			} else if ((exp[i] == ']') && (x != '[')) {
				return 0;
			} else if ((exp[i] == ')') && (x != '(')) {
				return 0;
			}
		}
	}
	if (isEmpty()) {
		return 1;
	} else {
		return 0;
	}
}

int main()
{
	char *exp1 = "((a+b)*(c-d))";
	char *exp2 = "(((a+b)*(c-d))";
	char *exp3 = "((a+b)*(c-d)))";
	char *exp4 = "{([a+b]*[c-d])/e}";
	char *exp5 = "{([a+b)*[c-d])/e}";

	printf("1 = Balanced, 0 = Not Balanced\n");
	printf("%s => %d \n", exp1, isBalanced(exp1));
	while (pop() > 0); // clearing previous stack values
	printf("\n\n");

	printf("%s => %d \n", exp2, isBalanced(exp2));
	while (pop() > 0); // clearing previous stack values
	printf("\n\n");

	printf("%s => %d \n", exp3, isBalanced(exp3));
	while (pop() > 0); // clearing previous stack values
	printf("\n\n");
	
	printf("%s => %d \n", exp4, isBalanced2(exp4));
	while (pop() > 0); // clearing previous stack values
	printf("\n\n");
	
	printf("%s => %d \n", exp5, isBalanced2(exp5));
	while (pop() > 0); // clearing previous stack values
	printf("\n\n");

	return 0;
}
