#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Program to convert Infix expression into a Postfix expression

/*
	Operator Precedence-Associativyty table
	Sym		Pre		Asso
	+,-		1		L-R
	*,/		2		L-R
	^		3		R-L
	-		4		R-L	Unary
	()		5		L-R
*/

/* Infix : a + b * c - d / e
   PostFix:
		   a + b * c - d / e
		   a + [bc*] - d / e
		   a + [bc*] - [de/]
		   [abc*+] - [de/]
		   
		   abc*+de/-
   
   PreFix:
		   a + b * c - d / e
		   a + [*bc] - d / e
		   a + [*bc] - [/de]
		   [+a*bc] - [/de]
		   
		   -+a*bc/de
*/

struct Node
{
	char data;
	struct Node *next;
}*top = NULL;

void push(char x)
{
	struct Node *t;
	
	t = (struct Node *)malloc(sizeof(struct Node));
	if (t == NULL) {
		printf("Stack is Full\n");
	} else {
		t->data = x;
		t->next = top;
		top = t;
	}

}

char pop()
{
	struct Node *t;
	char x = -1;

	if (top == NULL) {
		printf("Stack is Empty\n");
	} else {
		t = top;
		top = top->next;
		x = t->data;
		free(t);
	}
	return x;
}

int isEmpty()
{
	return top? 0 : 1;
}

int pre(char x)
{
	if (x == '+' || x == '-') {
		return 1;
	} else if (x == '*' || x == '/') {
		return 2;
	}
	return 0;
}

int isOperand(char x)
{
	if (x == '+' || x == '-' || x == '*' || x == '/') {
		return 0;
	} else {
		return 1;
	}
}

char * InfixToPostfix(char *infix)
{
	char *postfix;
	int len = strlen(infix);
	int i = 0, j = 0;
	postfix = (char *)malloc((len+2)*sizeof(char));

	while (infix[i] != '\0') {
		if (isOperand(infix[i])) {
			postfix[j++] = infix[i++];
		} else {
			if (pre(infix[i]) > pre(top->data)) {
				push(infix[i++]);
			} else {
				postfix[j++] = pop();
			}
		}
	}

	while (!isEmpty()) {
		postfix[j++] = pop();
	}
	postfix[j] = '\0';

	return postfix;
}

int main()
{
	char *infix = "a+b*c-d/e";
	push('#'); // This is to prevent error prev(top->data)

	char *postfix = InfixToPostfix(infix);

	printf("Infix : %s to Postfix : %s\n", infix, postfix);

	return 0;
}
