#include <iostream>
using namespace std;

// Program to implement stack using linked list

class Node
{
	public:
		int data;
		Node *next;
};

class Stack
{
	private:
		Node *top;

	public:
		Stack()
		{
			top = NULL;
		}

		void push(int x);
		int pop();
		void Display();
		int peek(int pos);
		int stackTop();
		int isEmpty();
		int isFull();
};

void Stack::push(int x)
{
	Node *t;
	
	t = new Node;
	if (t == NULL) {
		cout <<"Stack is Full" << endl;
	} else {
		t->data = x;
		t->next = top;
		top = t;
	}

}

int Stack::pop()
{
	Node *t;
	int x = -1;

	if (top == NULL) {
		cout << "Stack is Empty" << endl;
	} else {
		t = top;
		top = top->next;
		x = t->data;
		delete t;
	}
	return x;
}

int Stack::peek(int pos)
{
	int i;
	Node *p = top;

	for (i = 0; p != NULL && i < pos -1; i++) {
		p = p->next;
	}

	if (p != NULL) {
		return p->data;
	} else {
		return -1;
	}
}

int Stack::stackTop()
{
	if (top) {
		return top->data;
	}
	return -1;
}

int Stack::isEmpty()
{
	return top? 0 : 1;
}

int Stack::isFull()
{
	struct Node *t;

	t = (struct Node *)malloc(sizeof(struct Node));
	int r = !t? 1 : 0;
	free(t);
	return r;
}

void Stack::Display()
{
	Node *p;

	p = top;

	while (p != NULL) {
		cout << p->data << " ";
		p = p->next;
	}
	cout << endl;
}

int main()
{
	Stack stk;

	cout << "isEmpty : " << stk.isEmpty() << endl;
	stk.push(10);
	stk.push(20);
	stk.push(30);

	stk.Display();

	cout << "peek : " << stk.peek(1) << endl;
	cout << "peek : " << stk.peek(2) << endl;
	cout << "peek : " << stk.peek(3) << endl;
	cout << "peek : " << stk.peek(4) << endl;
	
	cout << "stackTop : " << stk.stackTop() << endl;
	cout << "isFull : " << stk.isFull() << endl;
	cout << "isEmpty : " << stk.isEmpty() << endl;

	cout << "Pop : " << stk.pop() << endl;
	stk.Display();

	return 0;
}
