#include <stdio.h>
#include <stdlib.h>

// Program to implement stack using linked list

struct Node
{
	int data;
	struct Node *next;
}*top = NULL;

void push(int x)
{
	struct Node *t;
	
	t = (struct Node *)malloc(sizeof(struct Node));
	if (t == NULL) {
		printf("Stack is Full\n");
	} else {
		t->data = x;
		t->next = top;
		top = t;
	}

}

int pop()
{
	struct Node *t;
	int x = -1;

	if (top == NULL) {
		printf("Stack is Empty\n");
	} else {
		t = top;
		top = top->next;
		x = t->data;
		free(t);
	}
	return x;
}

int peek(int pos)
{
	int i;
	struct Node *p = top;

	for (i = 0; p != NULL && i < pos -1; i++) {
		p = p->next;
	}

	if (p != NULL) {
		return p->data;
	} else {
		return -1;
	}
}

int stackTop()
{
	if (top) {
		return top->data;
	}
	return -1;
}

int isEmpty()
{
	return top? 0 : 1;
}

int isFull()
{
	struct Node *t;

	t = (struct Node *)malloc(sizeof(struct Node));
	int r = !t? 1 : 0;
	free(t);
	return r;
}

void Display()
{
	struct Node *p;

	p = top;

	while (p != NULL) {
		printf("%d ", p->data);
		p = p->next;
	}
	printf("\n\n");
}

int main()
{
	printf("isEmpty : %d\n", isEmpty());
	push(10);
	push(20);
	push(30);

	Display();

	printf("peek : %d\n", peek(1));
	printf("peek : %d\n", peek(2));
	printf("peek : %d\n", peek(3));
	printf("peek : %d\n", peek(4));
	
	printf("stackTop : %d\n", stackTop());
	printf("isFull : %d\n", isFull());
	printf("isEmpty : %d\n", isEmpty());

	printf("Pop : %d\n", pop());
	Display();

	return 0;
}
