#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Program to convert Infix expression into a Postfix expression

/*
	Operator Precedence-Associativyty table
	Sym		OutStackPre		InStackPrev		Asso
	+,-		1				2				L-R
	*,/		3				4				L-R
	^		6				5				R-L
	(		7				0				
	)		0				?				
*/

/*  Infix : ((a+b)*c)-d^e^f
	Postfix:
			([ab+]*c)-d^e^f
			[ab+c*]-d^e^f
			[ab+c*]-d^ef^ // Powe R-L
			[ab+c*]-def^^
			ab+c*def^^-
*/


struct Node
{
	char data;
	struct Node *next;
}*top = NULL;

void push(char x)
{
	struct Node *t;
	
	t = (struct Node *)malloc(sizeof(struct Node));
	if (t == NULL) {
		printf("Stack is Full\n");
	} else {
		t->data = x;
		t->next = top;
		top = t;
	}

}

char pop()
{
	struct Node *t;
	char x = -1;

	if (top == NULL) {
		printf("Stack is Empty\n");
	} else {
		t = top;
		top = top->next;
		x = t->data;
		free(t);
	}
	return x;
}

int isEmpty()
{
	return top? 0 : 1;
}

int OutStackPre(char x)
{
	if (x == '+' || x == '-') {
		return 1;
	} else if (x == '*' || x == '/') {
		return 3;
	} else if (x == '^') {
		return 6;
	} else if (x == '(') {
		return 7;
	} else if (x == ')') {
		return 0;
	}
	return 0;
}

int InStackPre(char x)
{
	if (x == '+' || x == '-') {
		return 2;
	} else if (x == '*' || x == '/') {
		return 4;
	} else if (x == '^') {
		return 5;
	} else if (x == '(') {
		return 0;
	}
	return 0;
}

int isOperand(char x)
{
	if (x == '+' || x == '-' || x == '*' || x == '/' || x == '^' || x == '(' || x == ')') {
		return 0;
	} else {
		return 1;
	}
}

char * InfixToPostfix(char *infix)
{
	char *postfix;
	int len = strlen(infix);
	int i = 0, j = 0;
	char x;
	postfix = (char *)malloc((len+2)*sizeof(char));

	while (infix[i] != '\0') {
		if (isOperand(infix[i])) {
			postfix[j++] = infix[i++];
		} else {
			if (OutStackPre(infix[i]) > InStackPre(top->data)) {
				push(infix[i++]);
			} else {
				x = pop();
				if (x == '(') {
					i++; // skip as (, ) matched and move to next symbol
				} else {
					postfix[j++] = x;
				}
			}
		}
	}

	while (!isEmpty()) {
		postfix[j++] = pop();
	}
	postfix[j] = '\0';

	return postfix;
}

int main()
{
	char *infix = "((a+b)*c)-d^e^f";
	push('#'); // This is to prevent error prev(top->data)

	char *postfix = InfixToPostfix(infix);

	printf("Infix : %s to Postfix : %s\n", infix, postfix);

	return 0;
}
