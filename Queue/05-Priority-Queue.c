#include <stdio.h>
#include <stdlib.h>

// Program to implement Priority Queues using array

/* 
	Priorities = 3
	Note: Smaller the number higher the priority
	Element -> 	A B C D E F G H I J
	Priority -> 1 1 2 3 4 1 2 3 2 2

	Element with priority 1 should gets pushed to Q1
	Element with priority 2 should gets pushed to Q2
	Element with priority 3 should gets pushed to Q3

	During delete element from Q1 first gets deleted, if Q1 is empty then only element from Q2 gets deleted, if it also empty then delete from Q3
*/

struct Queue
{
	int size;
	int front;
	int rear;
	char *Q;
};
	
struct Queue *q1, *q2, *q3;

struct Queue * create(int size)
{
	struct Queue *q;
	q = (struct Queue *)malloc(sizeof(struct Queue));
	q->size = size;
	q->front = q->rear = -1;
	q->Q = (char *)malloc(q->size * sizeof(char));

	return q;
}

int isEmpty(struct Queue q)
{
	if (q.front == q.rear) {
		return 1;
	}
	return 0;
}

int isFull(struct Queue q)
{
	if (q.rear == q.size-1) {
		return 1;
	}
	return 0;
}

void enqueue(char x, int priority)
{
	if ((priority == 1) && isFull(*q1)) {
		printf("Queue1 is Full\n");
	} else if ((priority == 2) && isFull(*q2)) {
		printf("Queue2 is Full\n");
	} else if ((priority == 3) && isFull(*q3)) {
		printf("Queue3 is Full\n");
	} else {
		if (priority == 1) {
			q1->rear++;
			q1->Q[q1->rear] = x;
		} else if (priority == 2) {
			q2->rear++;
			q2->Q[q2->rear] = x;
		} else if (priority == 3) {
			q3->rear++;
			q3->Q[q3->rear] = x;
		}
	}
}

char dequeue()
{
	char x = '\0';

	if (!isEmpty(*q1)) {
		q1->front++;
		x = q1->Q[q1->front];
	} else if (!isEmpty(*q2)) {
		q2->front++;
		x = q2->Q[q2->front];
	} else if (!isEmpty(*q3)) {
		q3->front++;
		x = q3->Q[q3->front];
	} else {
		printf("Queue is Empty\n");
	}
	return x;
}

void Display(struct Queue q)
{
	int i;

	for (i = q.front+1; i <= q.rear; i++) {
		printf("%c ", q.Q[i]);
	}
	printf("\n");
}

int main()
{

	q1 = create(5);
	q2 = create(5);
	q3 = create(5);

	enqueue('A', 1);
	enqueue('B', 1);
	enqueue('C', 2);
	enqueue('D', 3);
	enqueue('E', 2);
	enqueue('F', 1);
	enqueue('G', 2);
	enqueue('H', 3);
	enqueue('I', 2);
	enqueue('J', 2);

	printf("Q1:");
	Display(*q1);
	printf("Q2:");
	Display(*q2);
	printf("Q3:");
	Display(*q3);

	printf("isFull Q1: %d\n", isFull(*q1));
	printf("isFull Q2: %d\n", isFull(*q2));
	printf("isFull Q3: %d\n", isFull(*q3));

	for (int i = 0; i < 11; i++) {
		printf("\n");
		printf("Deleted : %c\n", dequeue());	
		printf("Q1:");
		Display(*q1);
		printf("Q2:");
		Display(*q2);
		printf("Q3:");
		Display(*q3);
	}

	return 0;
}
