#include <stdio.h>
#include <stdlib.h>

// Program to implement DEQueue using array

struct Queue
{
	int size;
	int front;
	int rear;
	int *Q;
};

void create(struct Queue *q, int size)
{
	q->size = size;
	q->front = q->rear = -1;
	q->Q = (int *)malloc(q->size * sizeof(int));
}

void InsertFromRear(struct Queue *q, int x)
{
	if (q->rear == q->size-1) {
		printf("Queue is Full\n");
	} else {
		q->rear++;
		q->Q[q->rear] = x;
	}
}

int DeleteFromRear(struct Queue *q)
{
	int x = -1;

	if (q->front == q->rear) {
		printf("Queue is Empty");
	} else {
		x = q->Q[q->rear];
		q->rear--;
	}
	return x;
}

void InsertFromFront(struct Queue *q, int x)
{
	if (q->front == -1) {
		printf("Can not insert more from front\n");
	} else {
		q->Q[q->front] = x;
		q->front--;
	}
}

int DeleteFromFront(struct Queue *q)
{
	int x = -1;

	if (q->front == q->rear) {
		printf("Queue is Empty");
	} else {
		q->front++;
		x = q->Q[q->front];
	}
	return x;
}

int isEmpty(struct Queue q)
{
	if (q.front == q.rear) {
		return 1;
	}
	return 0;
}

int isFull(struct Queue q)
{
	if (q.rear == q.size-1) {
		return 1;
	}
	return 0;
}

void Display(struct Queue q)
{
	int i;

	for (i = q.front+1; i <= q.rear; i++) {
		printf("%d ", q.Q[i]);
	}
	printf("\n\n");
}

int main()
{
	struct Queue q;

	create(&q, 5);

	InsertFromRear(&q, 10);
	InsertFromRear(&q, 20);
	InsertFromRear(&q, 30);
	InsertFromRear(&q, 40);
	InsertFromRear(&q, 50);
	Display(q);
	
	InsertFromFront(&q, 60);
	Display(q);

	printf("isFull : %d\n", isFull(q));

	printf("Deleted From Rear : %d\n", DeleteFromRear(&q));
	Display(q);
	
	printf("Deleted From Front : %d\n", DeleteFromFront(&q));
	Display(q);
	
	InsertFromFront(&q, 60);
	Display(q);

	return 0;
}
