#include <stdio.h>
#include <stdlib.h>

// Program to implement Queue using array

struct Queue
{
	int size;
	int front;
	int rear;
	int *Q;
};

void create(struct Queue *q, int size)
{
	q->size = size;
	q->front = q->rear = -1;
	q->Q = (int *)malloc(q->size * sizeof(int));
}

void enqueue(struct Queue *q, int x)
{
	if (q->rear == q->size-1) {
		printf("Queue is Full\n");
	} else {
		q->rear++;
		q->Q[q->rear] = x;
	}
}

int dequeue(struct Queue *q)
{
	int x = -1;

	if (q->front == q->rear) {
		printf("Queue is Empty");
	} else {
		q->front++;
		x = q->Q[q->front];
	}
	return x;
}

int isEmpty(struct Queue q)
{
	if (q.front == q.rear) {
		return 1;
	}
	return 0;
}

int isFull(struct Queue q)
{
	if (q.rear == q.size-1) {
		return 1;
	}
	return 0;
}

void Display(struct Queue q)
{
	int i;

	for (i = q.front+1; i <= q.rear; i++) {
		printf("%d ", q.Q[i]);
	}
	printf("\n\n");
}

int main()
{
	struct Queue q;

	create(&q, 5);

	enqueue(&q, 10);
	enqueue(&q, 20);
	enqueue(&q, 30);
	enqueue(&q, 40);
	enqueue(&q, 50);

	Display(q);
	printf("isFull : %d\n", isFull(q));

	printf("Deleted : %d\n", dequeue(&q));
	Display(q);

	return 0;
}
