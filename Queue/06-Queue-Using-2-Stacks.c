#include <stdio.h>
#include <stdlib.h>

// Program to implement queue using 2 stacks

struct Stack
{
	int size;
	int top;
	int *S;
};

struct Stack st1, st2;

void create(struct Stack *st)
{
	printf("Enter Size : ");
	scanf("%d", &st->size);
	st->top = -1;
	st->S = (int *)malloc(st->size * sizeof(int));
}

void Display(struct Stack st)
{
	int i;

	for (i = st.top; i >= 0; i--) {
		printf("%d ", st.S[i]);
	}
	printf("\n\n");
}

void push(struct Stack *st, int x)
{
	if (st->top == st->size - 1) {
		printf("Stack Overflow\n");
	} else {
		st->top++;
		st->S[st->top] = x;
	}
}

int pop(struct Stack *st)
{
	int x = -1;

	if (st->top == -1) {
		printf("Stack Underflow\n");
	} else {
		x = st->S[st->top--];
	}
	return x;
}

int isEmpty(struct Stack st)
{
	if (st.top == -1) {
		return 1;
	}
	return 0;
}

int isFull(struct Stack st)
{
	return st.top == st.size - 1;
}

void enqueue(int x)
{
	push(&st1, x);
}

int dequeue()
{
	int x = -1;

	if (isEmpty(st2)) {
		if (isEmpty(st1)) {
			printf("Queue Empty\n");
			return x;
		} else {
			while (!isEmpty(st1)) {
				push(&st2, pop(&st1));
			}
		}
	}
	return pop(&st2);
}

int main()
{
	create(&st1);
	
	create(&st2);

	enqueue(1);
	enqueue(2);
	enqueue(3);
	enqueue(4);
	enqueue(5);
	enqueue(6);

	printf("Deleted : %d\n", dequeue());
	printf("Deleted : %d\n", dequeue());
	printf("Deleted : %d\n", dequeue());

	return 0;
}
