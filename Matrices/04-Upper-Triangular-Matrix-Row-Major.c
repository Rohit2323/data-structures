#include <stdio.h>
#include <stdlib.h>

// Program to create Upper Triangular Matrix using Row Major Formula
// M[i, j] = 0 if i > j;
// M[i, j] = Non zero if i <= j
// Non-zero size = n(n+1)/2
// zero size = n(n-1)/2

// Index(A[i][j] = n(i-1) - (i-2)(i-1)/2 + j - i;

struct Matrix
{
	int *A;
	int n;
};

void Set(struct Matrix *m, int i, int j, int x)
{
	if (i <= j) {
		m->A[m->n * (i - 1) - (i - 2) * (i - 1) / 2 + j - i] = x;
	}
}

int Get(struct Matrix m, int i, int j)
{
	if (i <= j) {
		return m.A[m.n * (i - 1) - (i - 2) * (i - 1) / 2 + j - i];
	} else {
		return 0;
	}
}

void Display(struct Matrix m)
{
	int i, j;

	for (i = 1; i <= m.n; i++) {
		for (j = 1; j <= m.n; j++) {
			if (i <= j) {
				printf("%d ", m.A[m.n * (i - 1) - (i - 2) * (i - 1) / 2 + j - i]);
			} else {
				printf("0 ");
			}
		}
		printf("\n");
	}
}

int main()
{
	struct Matrix m;
	int i, j, x;

	printf("Enter Dimension: ");
	scanf("%d", &m.n);
	m.A = (int *)malloc(m.n * (m.n + 1) / 2 * sizeof(int));
	printf("Enter all the elements\n");
	for (i = 1; i <= m.n; i++) {
		for (j = 1; j <= m.n; j++) {
			scanf("%d", &x);;
			Set(&m, i, j, x);
		}
	}
	printf("\n\n");

	Display(m);

	free(m.A);

	return 0;
}
