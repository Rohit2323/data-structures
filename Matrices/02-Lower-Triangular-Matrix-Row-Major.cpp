#include <iostream>
using namespace std;

// Program to create Lower Triangular Matrix using Row Major Formula
// M[i, j] = 0 if i < j;
// M[i, j] = Non zero if i >= j
// Non-zero size = n(n+1)/2
// zero size = n(n-1)/2

// Index(A[i][j] = [i(i-1)/2] + j-1;

class LowerTri
{
	private:
		int *A;
		int n;

	public:
		LowerTri()
		{
			n = 2;
			A = new int[2 * (2 + 1) / 2];
		}

		LowerTri(int n)
		{
			this->n = n;
			A = new int[n * (n + 1) / 2];
		}

		~LowerTri()
		{
			delete []A;
		}

		void Set(int i, int j, int k);
		int Get(int i, int j);
		void Display();
		int GetDimension() { return n;}
};

void LowerTri::Set(int i, int j, int x)
{
	if (i >= j) {
		A[i * (i - 1) / 2 + j - 1] = x;
	}
}

int LowerTri::Get(int i, int j)
{
	if (i >= j) {
		return A[i * (i - 1) / 2 + j - 1];
	} else {
		return 0;
	}
}

void LowerTri::Display()
{
	int i, j;

	for (i = 1; i <= n; i++) {
		for (j = 1; j <= n; j++) {
			if (i >= j) {
				cout << A[i * (i - 1) / 2 + j - 1] << " ";
			} else {
				cout << "0 ";
			}
		}
		cout << endl;
	}
}

int main()
{
	int d, i, j;
	cout << "Enter Dimension : ";
	cin >> d;

	LowerTri lm(d);

	int x;
	cout << "Enter all Elements" << endl;
	for (i = 1; i <= d; i++) {
		for (j = 1; j <= d; j++) {
			cin >> x;
			lm.Set(i, j, x);
		}
	}
	cout << endl << endl;

	lm.Display();
	return 0;
}
