#include <stdio.h>

// Program to implement Heap Sort
// Time complexity - O(n log n)
// While storing in array 0th index is not used
/*
	element = i
	LChild = 2 * i
	RChild = 2 * i + 1
	Parent = |i/2|

	In place insertion of element in Heap array

	We can only delete root element from Heap

	Head is used to implementing priority queue
	a. Use max heap if larger number higher priority
	a. Use min heap if smarller number higher priority
*/

// n is index of element to be inserted
void Insert(int A[], int n)
{
	int i = n, temp;

	temp = A[i]; // value to be inserted in heap

	// Max heap : temp > A[i/2]
	// Min heap : temp < A[i/2]
	while (i > 1 && temp > A[i/2]) {
		A[i] = A[i/2];
		i = i/2;
	}
	A[i] = temp;
}

// We can only delete root i..e 1st element from Heap
int Delete(int A[], int n)
{
	int i, j, x, temp, val;

	val = A[1]; // Deleting element
	x = A[n];
	A[1] = A[n];
	A[n] = val; // storing in last place after heap
	i = 1; j = i*2;

	// As size = size - 1 after deletion
	while (j < n-1) {
		if (A[j+1] > A[j]) { // rchild > lchild
			j = j+1;
		}

		if (A[i] < A[j]) {
			// Swap
			temp = A[i];
			A[i] = A[j];
			A[j] = temp;

			i = j;
			j = 2*j;
		} else {
			break;
		}
	}
	return val;
}

void Display(int A[], int n)
{
	int i;

	for (i = 1; i <= n; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");
}

int main()
{
	int H[] = { 0, 10, 20, 30, 25, 5, 40, 35}; // O/P - 40 25 35 10 5 20 30
	int i;

	for (i = 2; i <= 7; i++) {
		Insert(H, i);
	}

	Display(H, 7);

	//printf("Deleted value is %d\n", Delete(H, 7));

	for (i = 7; i > 1; i--) {
		// Nothing but priority queue larger elemented delete 1st
		printf("Deleted value is %d\n", Delete(H, i));
	}
	printf("Heap Sort : ");
	Display(H, 7);

	return 0;
}
