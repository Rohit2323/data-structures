#include <iostream>
using namespace std;

// Program to implement Binary Tree using Queue

// Node if value is -1 means for left or rigth child

// Note: To use recursive function in class use dummy function

class Node
{
	public:
		Node *lchild;
		int data;
		Node *rchild;
};

/*********************** Circular Queue *****************************/

class Queue
{
	private:
		int size;
		int front;
		int rear;
		Node **Q;

	public:
		Queue()
		{
			front = rear = -1;
			size = 10;
			Q = new Node*[size];
		}

		Queue(int size)
		{
			front = rear = -1;
			this->size = size;
			Q = new Node*[this->size];
		}

		void enqueue(Node *x);
		Node * dequeue();
		int isEmpty();
		int isFull();
		void Display();
};

void Queue::enqueue(Node *x)
{
	if (rear == size-1) {
		printf("Queue is Full\n");
	} else {
		rear++;
		Q[rear] = x;
	}
}

Node * Queue::dequeue()
{
	Node *x = NULL;

	if (front == rear) {
		printf("Queue is Empty");
	} else {
		front++;
		x = Q[front];
	}
	return x;
}

int Queue::isEmpty()
{
	if (front == rear) {
		return 1;
	}
	return 0;
}

int Queue::isFull()
{
	if (rear == size-1) {
		return 1;
	}
	return 0;
}

/*********************** Tree *****************************/
class Tree
{
	private:
		Node *root;
	public:
		Tree()
		{
			root = NULL;
		}

		void TreeCreate();

		void Preorder()
		{
			Preorder(root); // To use recursion use of dummy function or overloaded function
		}
		void Preorder(Node *p);
		
		void Inorder()
		{
			Inorder(root);
		}
		void Inorder(Node *p);

		void Postorder()
		{
			Postorder(root);
		}
		void Postorder(Node *p);

		void Levelorder()
		{
			Levelorder(root);
		}
		void Levelorder(Node *p);

		int  Height()
		{
			return Height(root);
		}
		int  Height(Node *root);
};

void Tree::TreeCreate()
{
	Node *p, *t;
	int x;
	Queue q(100);

	cout << "Enter root value : ";
	cin >> x;
	root = new Node;
	root->data = x;
	root->lchild = root->rchild = NULL;
	q.enqueue(root); // Insert root in queue

	while (!q.isEmpty()) {
		p = q.dequeue(); // Remove node from queue

		// Left Child
		cout << "Enter left child of " << p->data << " : ";
		cin >> x;
		if (x != -1) {
			t = new Node;
			t->data = x;
			t->lchild = t->rchild = NULL;
			p->lchild = t; // IMP
			q.enqueue(t); // Insert root in queue
		}

		// Right Child
		cout << "Enter right child of " << p->data << " : ";
		cin >> x;
		if (x != -1) {
			t = new Node;
			t->data = x;
			t->lchild = t->rchild = NULL;
			p->rchild = t; // IMP
			q.enqueue(t); // Insert root in queue
		}
	}
}

// Root->Left->Right
void Tree::Preorder(Node *p)
{
	if (p) {
		cout << p->data << " ";
		Preorder(p->lchild);
		Preorder(p->rchild);
	}
}

// Left->Root->Right
void Tree::Inorder(Node *p)
{
	if (p) {
		Inorder(p->lchild);
		cout << p->data << " ";
		Inorder(p->rchild);
	}
}

// Left->Right->Root
void Tree::Postorder(Node *p)
{
	if (p) {
		Postorder(p->lchild);
		Postorder(p->rchild);
		cout << p->data << " ";
	}
}

int Tree::Height(Node *root)
{
	int x = 0, y = 0;

	if (root == NULL) {
		return 0;
	}

	x = Height(root->lchild);
	y = Height(root->rchild);

	if (x > y) {
		return x+1;
	} else {
		return y+1;
	}
}

void Tree::Levelorder(Node *root)
{
	Queue q(100);

	cout << root->data << " ";
	q.enqueue(root);

	while (!q.isEmpty()) {
		root = q.dequeue();
		if (root->lchild) {
			cout << root->lchild->data << " ";
			q.enqueue(root->lchild);
		}
		if (root->rchild) {
			cout << root->rchild->data << " ";
			q.enqueue(root->rchild);
		}
	}
}

int main()
{
	Tree t;
	t.TreeCreate();
	cout << "Preorder : ";
	t.Preorder();
	cout << endl;
	cout << "Inorder : ";
	t.Inorder();
	cout << endl;
	cout << "Postorder : ";
	t.Postorder();
	cout << endl;
	cout << "Levelorder : ";
	t.Levelorder();
	cout << endl;
	cout << "Height of Binary tree is : " << t.Height() << endl;

	return 0;
}
