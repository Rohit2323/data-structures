#include <stdio.h>
#include <stdlib.h>

// Program to implement Binary Tree using Queue

// Node if value is -1 means for left or rigth child
// Binary Tree Time complexity  : O(n)

/*
Enter root value : 8
Enter left child of 8 : 3
Enter right child of 8 : 5
Enter left child of 3 : 12
Enter right child of 3 : -1
Enter left child of 5 : 10
Enter right child of 5 : 6
Enter left child of 12 : -1
Enter right child of 12 : 4
Enter left child of 10 : -1
Enter right child of 10 : -1
Enter left child of 6 : 2
Enter right child of 6 : -1
Enter left child of 4 : 9
Enter right child of 4 : 7
Enter left child of 2 : -1
Enter right child of 2 : -1
Enter left child of 9 : -1
Enter right child of 9 : -1
Enter left child of 7 : -1
Enter right child of 7 : -1
Preorder Recursive: 8 3 12 4 9 7 5 10 6 2
Inorder Recursive: 12 9 4 7 3 8 10 5 2 6
Postorder Recursive: 9 7 4 12 3 10 2 6 5 8
Preorder Iterative: 8 3 12 4 9 7 5 10 6 2
Inorder Iterative: 12 9 4 7 3 8 10 5 2 6
Level Order: 8 3 5 12 10 6 4 2 9 7
Node Count : 10
Height : 5
Leaf Node Count : 4
Degree(2) Node Count : 3
Degree(1) Node Count : 3
Internal nodes = D(1) + D(2) Node Count : 6
*/

struct Node
{
	struct Node *lchild;
	int data;
	struct Node *rchild;
};

/*********************** Circular Queue *****************************/
struct Queue
{
	int size;
	int front;
	int rear;
	struct Node **Q;
};

void QueueCreate(struct Queue *q, int size)
{
	q->size = size;
	q->front = q->rear = 0; // IMP
	q->Q = (struct Node **)malloc(q->size * sizeof(struct Node *));
}

void enqueue(struct Queue *q, struct Node *x)
{
	if ((q->rear+1)%q->size == q->front) {
		printf("Queue is Full\n");
	} else {
		q->rear = (q->rear+1)%q->size;
		q->Q[q->rear] = x;
	}
}

struct Node * dequeue(struct Queue *q)
{
	struct Node *x = NULL;

	if (q->front == q->rear) {
		printf("Queue is Empty");
	} else {
		q->front = (q->front+1)%q->size;
		x = q->Q[q->front];
	}
	return x;
}

int isEmpty(struct Queue q)
{
	if (q.front == q.rear) {
		return 1;
	}
	return 0;
}

int isFull(struct Queue q)
{
	if (q.rear == q.size-1) {
		return 1;
	}
	return 0;
}

/*********************** Stack *****************************/
struct Stack
{
	int size;
	int top;
	struct Node **S;
};

void StackCreate(struct Stack *st, int size)
{
	st->size = size;
	st->top = -1;
	st->S = (struct Node **)malloc(st->size * sizeof(struct Node *));
}

void push(struct Stack *st, struct Node *x)
{
	if (st->top == st->size - 1) {
		printf("Stack Overflow\n");
	} else {
		st->top++;
		st->S[st->top] = x;
	}
}

struct Node * pop(struct Stack *st)
{
	struct Node *x = NULL;

	if (st->top == -1) {
		printf("Stack Underflow\n");
	} else {
		x = st->S[st->top--];
	}
	return x;
}

int isEmptyStack(struct Stack st)
{
	if (st.top == -1) {
		return 1;
	}
	return 0;
}

int isFullStack(struct Stack st)
{
	return st.top == st.size - 1;
}

/*********************** Tree *****************************/
struct Node *root = NULL;

void TreeCreate()
{
	struct Node *p, *t;
	int x;
	struct Queue q;
	QueueCreate(&q, 100);

	printf("Enter root value : ");
	scanf("%d", &x);
	root = (struct Node *)malloc(sizeof(struct Node));
	root->data = x;
	root->lchild = root->rchild = NULL;
	enqueue(&q, root); // Insert root in queue

	while (!isEmpty(q)) {
		p = dequeue(&q); // Remove node from queue

		// Left Child
		printf("Enter left child of %d : ", p->data);
		scanf("%d", &x);
		if (x != -1) {
			t = (struct Node *)malloc(sizeof(struct Node));
			t->data = x;
			t->lchild = t->rchild = NULL;
			p->lchild = t; // IMP
			enqueue(&q, t); // Insert root in queue
		}

		// Right Child
		printf("Enter right child of %d : ", p->data);
		scanf("%d", &x);
		if (x != -1) {
			t = (struct Node *)malloc(sizeof(struct Node));
			t->data = x;
			t->lchild = t->rchild = NULL;
			p->rchild = t; // IMP
			enqueue(&q, t); // Insert root in queue
		}
	}
}

/*********************** Recursive Traversal *****************************/
// Root->Left->Right
void preorder(struct Node *p)
{
	if (p) {
		printf("%d ", p->data);
		preorder(p->lchild);
		preorder(p->rchild);
	}
}

// Left->Root->Right
void inorder(struct Node *p)
{
	if (p) {
		inorder(p->lchild);
		printf("%d ", p->data);
		inorder(p->rchild);
	}
}

// Left->Right->Root
void postorder(struct Node *p)
{
	if (p) {
		postorder(p->lchild);
		postorder(p->rchild);
		printf("%d ", p->data);
	}
}

void Levelorder(struct Node *root)
{
	struct Queue q;
	QueueCreate(&q, 100);

	printf("%d ", root->data);
	enqueue(&q, root);

	while (!isEmpty(q)) {
		root = dequeue(&q);
		if (root->lchild) {
			printf("%d ", root->lchild->data);
			enqueue(&q, root->lchild);
		}
		if (root->rchild) {
			printf("%d ", root->rchild->data);
			enqueue(&q, root->rchild);
		}
	}
}
 
int count(struct Node *root)
{
	if (root) {
		return count(root->lchild) + count(root->rchild) + 1;
	}
	return 0;
}

/* Nodes with no left and right child also called as external nodes 
 * external = d(2) + 1
 */
int LeafNodes(struct Node *root)
{
	if (!root) {
		return 0;
	}
	if (!root->lchild && !root->rchild) {
		return LeafNodes(root->lchild) + LeafNodes(root->rchild) + 1;
	} else {
		return LeafNodes(root->lchild) + LeafNodes(root->rchild);
	}
}

/* Nodes with both left and right child */
int Degree2Nodes(struct Node *root)
{
	if (!root) {
		return 0;
	}
	if (root->lchild && root->rchild) {
		return Degree2Nodes(root->lchild) + Degree2Nodes(root->rchild) + 1;
	} else {
		return Degree2Nodes(root->lchild) + Degree2Nodes(root->rchild);
	}
}

/* Nodes with either left or right child */
int Degree1Nodes(struct Node *root)
{
	if (!root) {
		return 0;
	}
	if (root->lchild != NULL ^ root->rchild != NULL) {
		return Degree1Nodes(root->lchild) + Degree1Nodes(root->rchild) + 1;
	} else {
		return Degree1Nodes(root->lchild) + Degree1Nodes(root->rchild);
	}
}

/* Internal nodes means d(1) + d(2) */
int InternalNodes(struct Node *root)
{
	if (!root) {
		return 0;
	}
	if (root->lchild || root->rchild) {
		return InternalNodes(root->lchild) + InternalNodes(root->rchild) + 1;
	} else {
		return InternalNodes(root->lchild) + InternalNodes(root->rchild);
	}
}

int height(struct Node *root)
{
	int x = 0, y = 0;

	if (root == NULL) {
		return 0;
	}

	x = height(root->lchild);
	y = height(root->rchild);
	if (x > y) {
		return x+1;
	} else {
		return y+1;
	}
}

/*********************** Interative Traversal *****************************/
void IPreorder(struct Node *p)
{
	struct Stack stk;
	StackCreate(&stk, 100);

	while (p || !isEmptyStack(stk)) {
		if (p) {
			printf("%d ", p->data);
			push(&stk, p);
			p = p->lchild;
		} else {
			p = pop(&stk);
			p = p->rchild;
		}
	}
}

void IInorder(struct Node *p)
{
	struct Stack stk;
	StackCreate(&stk, 100);

	while (p || !isEmptyStack(stk)) {
		if (p) {
			push(&stk, p);
			p = p->lchild;
		} else {
			p = pop(&stk);
			printf("%d ", p->data);
			p = p->rchild;
		}
	}
}

int main()
{
	TreeCreate();
	printf("Preorder Recursive: ");
	preorder(root);
	printf("\n");
	printf("Inorder Recursive: ");
	inorder(root);
	printf("\n");
	printf("Postorder Recursive: ");
	postorder(root);
	printf("\n");
	
	printf("Preorder Iterative: ");
	IPreorder(root);
	printf("\n");
	printf("Inorder Iterative: ");
	IInorder(root);
	printf("\n");
	
	printf("Level Order: ");
	Levelorder(root);
	printf("\n");

	printf("Node Count : %d\n", count(root));
	printf("Height : %d\n", height(root));
	
	printf("Leaf/External Node Count : %d\n", LeafNodes(root));
	printf("Degree(2) Node Count : %d\n", Degree2Nodes(root));
	printf("Degree(1) Node Count : %d\n", Degree1Nodes(root));
	printf("Internal nodes = D(1) + D(2) Node Count : %d\n", InternalNodes(root));
	
	return 0;
}
